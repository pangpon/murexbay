﻿

'-------------------------------------------------------------------------'
'----------------------- TEST SCRIPT Start--------------------------------'
'-------------------------------------------------------------------------'
'iRowCount = Datatable.getSheet("TestStep").getRowCount
'DTRow = 1 To DataTable.GlobalSheet.GetRowCount
iRowCount =  Datatable.getSheet("TC001 [TC001]").getRowCount
	For i =  1 To iRowCount 'Step 1
		DataTable.LocalSheet.SetCurrentRow(i)
		'TagAction=Ucase(DataTable("Tag",dtglobalsheet))
		User=trim((DataTable("Username","TC001 [TC001]")))
		Password=trim((DataTable("Password","TC001 [TC001]")))


'--------------------------Login - Using Library Function-------------------------'

Services.StartTransaction "TC_01_001_Login"
 @@ hightlight id_;_2079360520_;_script infofile_;_ZIP::ssf13.xml_;_
'GettextOnedit "Micro Focus MyFlight Sample","John Smith"
DPWebEdit "Micro Focus MyFlight Sample","agentName", User
DPWebEdit "Micro Focus MyFlight Sample","password", Password

Services.EndTransaction "TC_01_001_Login"

Next
'-------------------------------------------------------------------------'
'----------------------- TEST SCRIPT END----------------------------------'
'-------------------------------------------------------------------------'

'WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click

 
' WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentName").Set

'Browser("Google").Page("Google").WebEdit("Search").Set
'WpfWindow("Micro Focus MyFlight Sample").WpfComboBox("fromCity").Select
 @@ hightlight id_;_1909995384_;_script infofile_;_ZIP::ssf16.xml_;_
'WpfWindow("Micro Focus MyFlight Sample").WpfComboBox("fromCity").Select "Paris" @@ hightlight id_;_2081815664_;_script infofile_;_ZIP::ssf20.xml_;_


'6/13/2019 @@ hightlight id_;_1943483528_;_script infofile_;_ZIP::ssf29.xml_;_
'WpfWindow("Micro Focus MyFlight Sample").WpfCalendar("datePicker").SetDate "13-Jun-2019" @@ hightlight id_;_1948523432_;_script infofile_;_ZIP::ssf30.xml_;_

'WpfWindow("Micro Focus MyFlight Sample").WpfCalendar("datePicker").SetDate "10/20/2019"
