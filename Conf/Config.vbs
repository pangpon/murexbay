'#######################################################################################
'# Script Name				: 
'# Script Description 		: This script contents global variables and file variables. 
'# Version Information		: Phase	XXX 
'# Client Name				: 
'# Date Created  			: DD/MM/YYYY 
'# Author					: 
'# Function					: 	 
'#######################################################################################
Option Explicit
Public fso					'	Parameter Create Object for Manage File
Public objExcel
Public objWorkBook
Public objWorkSheet
Public objWorkSheetDeposit
Public objWorkSheetBatch
Public objWorkSheetInstrument
Public objChildExcel
Public objChWorkBook
Public objChWorkSheet

Public MyFile
Public TestCase
Public DataRow
Public StartExecuteDate
Public StartExecuteTime
Public StoptExecuteTime
'Public StartTimeExecuted
'Public StopTimeExecuted
'Public MyMstResultFile
'Public Action
'Public Status 
'Public strBrowser
'Public strPage
'Public strFrame
'public strMsgTxt
'public strFileName
'public strSheetName
'public strChildSheetName
'Public gstrOutputFile

'Navigation
Public obj_Browser			'	Create Object Browser
Public obj_Page				'	Create Object Page
Public obj_WebTable			'	Create Object WebTable
Public strWebLink			'	String Define Object WebLink
Public strWebButton			'	String Define Object WebButton
Public strCellData			'	WebTable Get Cell Data


'Iteration Variables
Public ItrCnt
Public InstCnt

'Constant Declaration for Wait Property
Const Wait_Short = 2
Const Wait_Medium = 5
Const Wait_Long = 10
Const Wait_half = 30
Const Wait_Double = 60

'Constant Declaration for General
Const Str_Delimiter="|"
Public strResultPathExc	'Parameter for Create Folder SnapShot & Define Result name
Public strResultFileExc	'Parameter for Create Folder SnapShot & Define Result name
Public strResultPathTestCase	'Parameter for Create Folder TestCase Result & SnapShot
Public FileNameResult	'Parameter for Create File Result Export from DataTable
Public strResultPathTestCase  'Parameter for Create Folder SnapShot & Define TMP Result name
Const CaptureSS_Status="Capture"	'	Flag Capture SnapShot Result Fail (True , Capture )

'	Excel Config
'	Path TestData
'Const Path_TestData="D:\Automate\WebTours\TestData\"
'Const Path_TestData="D:\murexframework\Data"
Const Path_TestData="D:\MurexBay\Data"    ' note PC

Const TestData_Input="TestData.xlsx"
'Const Path_Result="D:\murexframework\Result\"
Const Path_Result= "D:\MurexBay\Result"  'note PC
Const Path_Tmp_Result= strFolderpath&"\"&"TMP"
Const L_FilenameResult="_Result"
'Const ExcelType=".xlsx"

'Constant Declaration for Excel Result
Const str_DT_ResultStatus="Status"		'string Column DataTable Result Status
Const str_DT_ResultDes="Desctiption"		'string Column DataTable Result Description
Public strResultDes
Public strResultStatus

'------------------------------ Scenario Control Started -----------------------------------------'
Public StrScenario
Public StrTag
'------------------------------ Scenario Control End -----------------------------------------'

'------------------------------ MCN (Get Content log from Mobile SMS) Application Parameter Started -----------------------------------------'
'	URL 
Const WebURL_MCN = ""





'------------------------------ User Modification End ---------------------------------------------'


