Option Explicit

Public sub app_Init(strWpfWindow)
   	On error resume next  
	obj_WpfWindow=strWpfWindow
	'obj_Page=strPage
'WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentName").Set
   If WpfWindow(obj_WpfWindow).Exist(0) Then    				   
		If err.number <>0 Then
			PrintInfo "micFail", "WpfWindow", " Error Description: " & Err.Description			  
			Err.clear						
			Exit sub
		End If
   	Else
	   PrintInfo "micFail", "WpfWindow", " Browser not found: " & strBrowser & " - " & strPage 	    
    End If             	
End sub

'Public sub app_frame(strInValue)
'   	On error resume next  
'	Set obj_Frame = Description.Create  	
'	obj_Frame("name").value= strInValue
'    	
'    If Browser(obj_Browser).Page(obj_Page).Frame(obj_Frame).Exist(0) Then    			   
'		If err.number <>0 Then
'			PrintInfo "micFail", "Frame", " Error Description: " & Err.Description			  
'			Err.clear						
'			Exit sub
'		End If
'   	Else
'	   PrintInfo "micFail", "Frame", " Frame not found: " 	    
'    End If        	                                                                       
'End sub


Public Sub DPWebEdit(obj_WpfWindow,strWebEdit,strInValue)



	On error resume next
	Call StartTime(StrStartTime)
	If strInValue <> "" Then
	'WpfWindow("Micro Focus MyFlight Sample").WpfEdit("agentName").Set
	
		With WpfWindow(obj_WpfWindow)			
   			If .WpfEdit(strWebEdit).Exist(0) Then
   			   .WpfEdit(strWebEdit).highlight  			   
   			   .WpfEdit(strWebEdit).Set strInValue   
  				Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", strWebEdit, strWebEdit & "WebEdit not found."   
   			    StrStatus = strWebEdit & "Step is False"
   			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime) 			   
         	End If         	
		End With			
	End If
End Sub 

'--------------------------------------------------------------------------------------------

Public Sub DPWebList (strWebList, strInValue)    
	On error resume next
	If strInValue <> "" Then
		With Browser(obj_Browser).Page(obj_Page)						
			If .WebList(strWebList).Exist(0) Then
				.WebList(strWebList).Select strInValue
				If err.number <>0 Then
        			PrintInfo "micFail", strInValue,  " Error Description: " & Err.Description
        			Err.clear						
					Exit sub
				End If
			Else
   			   PrintInfo "micFail", strWebList,  strWebList & " - WebList not found."
         	End If			
		End With		
	End If
End Sub

Public Sub DPWebList2 (strWebList, strInValue)    
	On error resume next
	If strInValue <> "" Then
		With Browser(obj_Browser).Page(obj_Page).Frame(obj_Frame)						
			If .WebList("name:=" & strWebList).Exist(0) Then
				.WebList("name:=" & strWebList).Select strInValue
				If err.number <>0 Then
        			PrintInfo "micFail", strInValue,  " Error Description: " & Err.Description
        			Err.clear						
					Exit sub
				End If
			Else
   			   PrintInfo "micFail", strWebList,  strWebList & " - WebList not found."
         	End If			
		End With		
	End If
End Sub

Public Sub DPWebElement(strType,strInValue)
	On error resume next	
		With Browser(obj_Browser).Page(obj_Page).Frame(obj_Frame)
   			If .WebElement(strType & ":=" & strInValue).Exist(0) Then
   			   .WebElement(strType & ":=" & strInValue).Click
   				If err.number <>0 Then
        			PrintInfo "micFail", strInValue,  " Error Description: " & Err.Description
        			Err.clear						
					Exit sub
				End If
   			Else
   			  	PrintInfo "micFail", strInValue , strInValue & " - WebElement not found."
         	End If
		End With
End Sub


Public Function GetWebElement2(strObjp,strInValue,strType)
	On error resume next	
		With Browser(obj_Browser).Page(obj_Page).Frame(obj_Frame)
   			If .WebElement(strObjp & ":=" & strInValue).Exist(0) Then
   			   GetWebElement = .WebElement(strObjp & ":=" & strInValue).GetROProperty(strType)
   				If err.number <>0 Then
        			PrintInfo "micFail", strInValue,  " Error Description: " & Err.Description
        			Err.clear						
					Exit Function
				End If
   			Else
   			  	PrintInfo "micFail", strInValue , strInValue & " - WebElement not found."
         	End If         	
		End With
End Function

'--------------------------------------------------------------------------------------------

Public Sub DPWebLink_Click(obj_WpfWindow,strWebLink)
	On error resume next
	If strWebLink <> "" Then
		With WpfWindow(obj_WpfWindow)			
   			If .WpfButton(strWebLink).Exist(0) Then
   			   .WpfButton(strWebLink).click   			
			   	If err.number <>0 Then
        			PrintInfo "micFail", "WebLink Click", strWebLink & " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)
        			Err.clear						
					Exit sub
				End If
   			Else
   			   PrintInfo "micFail", "WebLink Click", strWebLink & " WebLink not found."   			   
         	End If         	
		End With			
	End If
End Sub

Public Sub DPWebLink_MultiDes_Click(strWebLink01,strWebLink02)
	On error resume next
	If strWebLink01 <> "" and strWebLink02 <> "" Then
		With Browser(obj_Browser).Page(obj_Page)			
   			If .Link(strWebLink01,strWebLink02).Exist(2) Then
   			   .Link(strWebLink01,strWebLink02).click   			
			   	If err.number <>0 Then
        			PrintInfo "micFail", "WebLink Click", strWebLink01 & strWebLink02 & " Error Description: " & Err.Description
        			Err.clear						
					Exit sub
				End If
   			Else
   			   PrintInfo "micFail", "WebLink Click", strWebLink01 & strWebLink02 & " WebLink not found."   			   
         	End If         	
		End With			
	End If
End Sub

'--------------------------------------------------------------------------------------------
'	WebTable

Public Sub DPWebTable_GetCellData(strWebTable,intRow, intCol)
	On error resume next	           	                    
	With Browser(obj_Browser).Page(obj_Page)                                                                 
		If .WebTable(strWebTable).Exist(0) Then
			strCellData = trim(.WebTable(strWebTable).GetCellData (intRow, intCol))
			If err.number <>0 Then
				PrintInfo "micFail", strWebTable,  " Error Description: " & Err.Description
				Err.clear                                                                                               
				Exit sub
			End If
		Else
	   		PrintInfo "micFail", strWebTable, strWebTable & " WebTable not found: " 	                                      
		End If                    
	End With                              
End Sub
'--------------------------------------------------------------------------------------------


Public Sub DPWebButton_Click(obj_WpfWindow,strWebEdit)
	On error resume next
	Call StartTime(StrStartTime)
	'WpfWindow("Micro Focus MyFlight Sample").WpfButton("OK").Click

	If strWebEdit <> "" Then
		With WpfWindow(obj_WpfWindow)			
   			If .WpfButton(strWebEdit).Exist(0) Then
   			   .WpfButton(strWebEdit).click   		
        			Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", "WebButton Click", strWebEdit & " WebButton not found." 
			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)    			   
         	End If         	
		End With			
	End If
End Sub

'	Web Button Click Define 2 Object Description
Public Sub DPWebButton_MultiDes_Click(strWebButton01,strWebButton02)
	On error resume next
	If strWebButton01 <> "" and strWebButton02 <> "" Then
		With Browser(obj_Browser).Page(obj_Page)			
   			If .WebButton(strWebButton01,strWebButton02).Exist(0) Then
   			   .WebButton(strWebButton01,strWebButton02).click   			
			   	If err.number <>0 Then
        			PrintInfo "micFail", "WebButton Click", strWebButton01 & strWebButton02& " Error Description: " & Err.Description
        			Err.clear						
					Exit sub
				End If
   			Else
   			   PrintInfo "micFail", "WebButton Click", strWebButton01 & strWebButton02 & " WebButton not found."   			   
         	End If         	
		End With			
	End If
End Sub

'--------------------------------------------------------------------------------------------
'	Get Ro Property Description
Public Function DP_Obj_GetROProperty(strObject,strObjectValue,strProperty)
	On error resume next
	If strObject <> "" and strProperty <> ""  Then

		If Lcase(strObject) = "link" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Link(strObjectValue).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.Link(strObjectValue).GetROProperty(strProperty)  
					DP_Obj_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue & " WebLink cannot GetProperty " &strProperty   			   
	         	End If         	
			End With		
			
		ELseIf Lcase(strObject) = "webelement" Then'or Lcase(strObject) = "element" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebElement(strObjectValue).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.WebElement(strObjectValue).GetROProperty(strProperty)  
					DP_Obj_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue & " WebElement cannot GetProperty " &strProperty   			   
	         	End If         	
			End With		
			
		ELseIf Lcase(strObject) = "webbutton" or Lcase(strObject) = "button" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebButton(strObjectValue).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.WebButton(strObjectValue).GetROProperty(strProperty)  
					DP_Obj_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue & " WebButton cannot GetProperty " &strProperty   			   
	         	End If         	
			End With	
			
		ELseIf Lcase(strObject) = "webedit" or Lcase(strObject) = "edit" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebEdit(strObjectValue).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.WebEdit(strObjectValue).GetROProperty(strProperty)  
					DP_Obj_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue & " WebEdit cannot GetProperty " &strProperty   			   
	         	End If         	
			End With	
		ELseIf Lcase(strObject) = "webpage" or Lcase(strObject) = "page" Then
	   			If Browser(obj_Browser).Page(obj_Page).Exist(0) Then	    			
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=Browser(obj_Browser).Page(obj_Page).GetROProperty(strProperty)  
					DP_Obj_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue & " WebPage cannot GetProperty " &strProperty   			   
	         	End If  

		ELseIf Lcase(strObject) = "image" Then' or Lcase(strObject) = "img" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Image(strObjectValue).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.Image(strObjectValue).GetROProperty(strProperty)  
					DP_Obj_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue & " Image cannot GetProperty " &strProperty   			   
	         	End If         	
			End With	

		End If

	End If
End Function


'	Get Ro Property Multi Description
Public Function DP_Obj_MultiDes_GetROProperty(strObject,strObjectValue01,strObjectValue02,strProperty)
	On error resume next
	If strObject <> "" and strObjectValue01 <> "" and strObjectValue02 <> "" Then

		If Lcase(strObject) = "weblink" or Lcase(strObject) = "link" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Link(strObjectValue01,strObjectValue02).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.Link(strObjectValue01,strObjectValue02).GetROProperty(strProperty)  
					DP_Obj_MultiDes_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " WebLink cannot GetProperty " &strProperty   			   
	         	End If         	
			End With		
			
		ELseIf Lcase(strObject) = "webelement" or Lcase(strObject) = "element" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebElement(strObjectValue01,strObjectValue02).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.WebElement(strObjectValue01,strObjectValue02).GetROProperty(strProperty)  
					DP_Obj_MultiDes_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " WebElement cannot GetProperty " &strProperty   			   
	         	End If         	
			End With		
			
		ELseIf Lcase(strObject) = "webbutton" or Lcase(strObject) = "button" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebButton(strObjectValue01,strObjectValue02).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.WebButton(strObjectValue01,strObjectValue02).GetROProperty(strProperty)  
					DP_Obj_MultiDes_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " WebButton cannot GetProperty " &strProperty   			   
	         	End If         	
			End With	
			
		ELseIf Lcase(strObject) = "webedit" or Lcase(strObject) = "edit" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebEdit(strObjectValue01,strObjectValue02).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.WebEdit(strObjectValue01,strObjectValue02).GetROProperty(strProperty)  
					DP_Obj_MultiDes_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " WebEdit cannot GetProperty " &strProperty   			   
	         	End If         	
			End With	
			
		ELseIf Lcase(strObject) = "image" or Lcase(strObject) = "img" Then

			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Image(strObjectValue01,strObjectValue02).Exist(0) Then
				   	If err.number <>0 Then
	        			PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " Error Description: " & Err.Description
	        			Err.clear						
						Exit Function
					End If
					str_Obj_value=.Image(strObjectValue01,strObjectValue02).GetROProperty(strProperty)  
					DP_Obj_MultiDes_GetROProperty=str_Obj_value
					
	   			Else
	   			   PrintInfo "micFail", strObject, strObjectValue01 & strObjectValue02 & " Image cannot GetProperty " &strProperty   			   
	         	End If         	
			End With	
			
		End If

	End If
End Function


'	Check Object and click
Public Sub DP_Obj_Exist_Click(strObject,strObjectValue,strWaitTime)
	On error resume next
	If strWaitTime <> "" Then
		strWaitTime=Wait_Short
	End If
	
	If strObject <> "" and strObjectValue <> "" Then
	
		If Lcase(strObject) = "weblink" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebLink(strObjectValue).Exist(strWaitTime) Then 			
					.WebLink(strObjectValue).click 
	         	End If         	
			End With
		Elseif Lcase(strObject) = "link" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Link(strObjectValue).Exist(strWaitTime) Then 			
					.Link(strObjectValue).click 
	         	End If         	
			End With	
		Elseif Lcase(strObject) = "image" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Image(strObjectValue).Exist(strWaitTime) Then 			
					.Image(strObjectValue).click 
	         	End If         	
			End With
		Elseif Lcase(strObject) = "link" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Link(strObjectValue).Exist(strWaitTime) Then 			
					.Link(strObjectValue).click 
	         	End If         	
			End With
		Elseif Lcase(strObject) = "webelement" or Lcase(strObject) = "element" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebElement(strObjectValue).Exist(strWaitTime) Then 			
					.WebElement(strObjectValue).click 
	         	End If         	
			End With
		Elseif Lcase(strObject) = "webbutton" or Lcase(strObject) = "button" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebButton(strObjectValue).Exist(strWaitTime) Then 			
					.WebButton(strObjectValue).click 
	         	End If         	
			End With
		Elseif Lcase(strObject) = "webcheckbox" or Lcase(strObject) = "checkbox" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebCheckBox(strObjectValue).Exist(strWaitTime) Then 			
					.WebCheckBox(strObjectValue).click 
	         	End If         	
			End With
		End If
	Else
			Print "Fail to call Function "&strObject&" "&strObjectValue
	 		Exit sub
	End If
	
	
End Sub


'--------------------------------------------------------------------------------------------
'	Check Object Exist
Public Function DP_Obj_Exist(strObject,strObjectValue,strWaitTime)
	On error resume next
	If strWaitTime = "" Then
		strWaitTime=Wait_Short
	End If
	
	If strObject <> "" and strObjectValue <> "" Then
		
		'	Set Default Value return Object status
		DP_Obj_Exist="Exist"
		If Lcase(strObject) = "webedit" or Lcase(strObject) = "edit" Then
			
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebEdit(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebEdit Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebEdit Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Not Exist"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		ElseIf Lcase(strObject) = "weblink" or Lcase(strObject) = "link" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .Link(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebLink Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebLink Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "weblist" or Lcase(strObject) = "list" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebList(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebList Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebList Error Description: " & Err.Description,"")
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "webelement" or Lcase(strObject) = "element" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebElement(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebElement Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebElement Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "webbutton" or Lcase(strObject) = "button" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebButton(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebButton Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebButton Error Description: " & Err.Description,"")
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		Elseif Lcase(strObject) = "webtable" or Lcase(strObject) = "table" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebTable(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebTable Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebTable Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		Elseif Lcase(strObject) = "image" or Lcase(strObject) = "img" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .Image(strObjectValue).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "Image Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "Image Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Else
			Print "Fail to call Function "&strObject&" "&strObjectValue
			DP_Obj_Exist="Fail"
	 		Exit Function
	 	End If
	End If
	

	
End Function


'	Check Object Multi Description Exist
Public Function DP_Obj_MultiDes_Exist(strObject,strObjectValue01,strObjectValue02,strWaitTime)
	On error resume next
	If strWaitTime = "" Then
		strWaitTime=Wait_Short
	End If
	
	If strObject <> "" and strObjectValue01 <> "" and strObjectValue02 <> "" Then
		
		'	Set Default Value return Object status
		DP_Obj_Exist="Exist"
		If Lcase(strObject) = "webedit" or Lcase(strObject) = "edit" Then
			
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebEdit(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "WebEdit Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "WebEdit Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Not Exist"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		ElseIf Lcase(strObject) = "weblink" or Lcase(strObject) = "link" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .Link(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "WebLink Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "WebLink Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "weblist" or Lcase(strObject) = "list" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebList(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "WebList Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "WebList Error Description: " & Err.Description,"")
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "webelement" or Lcase(strObject) = "element" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebElement(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "WebElement Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "WebElement Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "webbutton" or Lcase(strObject) = "button" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebButton(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "WebButton Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "WebButton Error Description: " & Err.Description,"")
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		Elseif Lcase(strObject) = "webtable" or Lcase(strObject) = "table" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebTable(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "WebTable Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "WebTable Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		Elseif Lcase(strObject) = "image" or Lcase(strObject) = "img" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .Image(strObjectValue01,strObjectValue02).Exist(strWaitTime) Then 			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue01&strObjectValue02,  "Image Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue01&strObjectValue02,  "Image Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Else
			Print "Fail to call Function "&strObject&" "&strObjectValue
			DP_Obj_Exist="Fail"
	 		Exit Function
	 	End If
	End If
	

	
End Function


'--------------------------------------------------------------------------------------------
'--------------------------------------------------------------------------------------------
'	Check Object Sync
Public Function DP_Obj_Sync(strObject,strObjectValue,strWaitTime)
	On error resume next
	If strWaitTime = "" Then
		strWaitTime=Wait_Short
	End If
	
	If strObject <> "" Then
		'	Set Default Value return Object status
		DP_Obj_Exist="Exist"
		If Lcase(strObject) = "webpage" or Lcase(strObject) = "page" Then
					
	   		If Browser(obj_Browser).Page(obj_Page).Exist(strWaitTime) Then 
				Browser(obj_Browser).Page(obj_Page).sync					
			Else
				If err.number <>0 Then
        			PrintInfo "micFail", strObject&" "&strObjectValue,  "Browser-Page Sync Error Description: " & Err.Description
        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "Browser-Page Sync Error Description: " & Err.Description,"")
        			DP_Obj_Exist="Fail"
        			Err.clear						
					Exit Function
				End If 	
         	End If         	

		ElseIf Lcase(strObject) = "webedit" or Lcase(strObject) = "edit" Then
			
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebEdit(strObjectValue).Exist(strWaitTime) Then 
					.WebEdit(strObjectValue).sync					
		   		Else
			   		If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebEdit Sync Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebEdit Sync Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		ElseIf Lcase(strObject) = "weblink" or Lcase(strObject) = "link" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .Link(strObjectValue).Exist(strWaitTime) Then 	
					.Link(strObjectValue).sync	
				Else					
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebLink Sync Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebLink Sync Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "weblist" or Lcase(strObject) = "list" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebList(strObjectValue).Exist(strWaitTime) Then 	
					.WebList(strObjectValue).sync	   		
				Else					
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebList Sync Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebList Sync Error Description: " & Err.Description,"")
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "webelement" or Lcase(strObject) = "element" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebElement(strObjectValue).Exist(strWaitTime) Then 	
					.WebElement(strObjectValue).sync	   			
		   		Else
			   		If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebElement Sync Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebElement Sync Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Elseif Lcase(strObject) = "webbutton" or Lcase(strObject) = "button" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If Not .WebButton(strObjectValue).Exist(strWaitTime) Then 	
					.WebButton(strObjectValue).sync	   			
		   			If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebButton Sync Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebButton Sync Error Description: " & Err.Description,"")
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With

		Elseif Lcase(strObject) = "webtable" or Lcase(strObject) = "table" Then
			With Browser(obj_Browser).Page(obj_Page)			
	   			If .WebTable(strObjectValue).Exist(strWaitTime) Then 
					.WebTable(strObjectValue).sync	   			
		   		Else
			   		If err.number <>0 Then
	        			PrintInfo "micFail", strObject&" "&strObjectValue,  "WebTable Sync Error Description: " & Err.Description
	        			Call Fnc_ExitIteration(strObject&" "&strObjectValue,  "WebTable Sync Error Description: " & Err.Description,"")
	        			DP_Obj_Exist="Fail"
	        			Err.clear						
						Exit Function
					End If 	
	         	End If         	
			End With
			
		Else
			Print "Fail to call Function "&strObject&" "&strObjectValue
			DP_Obj_Exist="Fail"
	 		Exit Function
	 	End If
	End If
	
End Function

' Oject WebRadioGroup
Public Sub DPWebRadioGroup(strWebRadioGroup, strInValue)
	On error resume next
	If strInValue <> "" Then
		With Browser(obj_Browser).Page(obj_Page)			
   			If .WebRadioGroup(strWebRadioGroup).Exist(0) Then
   			   .WebRadioGroup(strWebRadioGroup).Select strInValue   			
			   	If err.number <>0 Then
        			PrintInfo "micFail", strWebRadioGroup,  " Error Description: " & Err.Description
        			Err.clear						
					Exit sub
				End If
   			Else
   			   PrintInfo "micFail", strWebRadioGroup, strWebRadioGroup & "strWebRadioGroup not found."   			   
         	End If         	
		End With			
	End If
End Sub 



Public Sub DPSelect_Combo(obj_WpfWindow,strWebEdit,strInValue)
	On error resume next
	Call StartTime(StrStartTime)
	If strInValue <> "" Then

		'WpfWindow("Micro Focus MyFlight Sample").WpfComboBox("fromCity").Select "Paris"
		With WpfWindow(obj_WpfWindow)			
   			If .WpfComboBox(strWebEdit).Exist(0) Then
   			   .WpfComboBox(strWebEdit).select strInValue   			
			   		Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)
        			Err.clear						
					Exit sub
   			Else
   			   	PrintInfo "micFail", strWebEdit, strWebEdit & "WebEdit not found."   
   			    StrStatus = strWebEdit & "Step is False"
   			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime) 				   
         	End If         	
		End With			
	End If
End Sub 


'Public Sub Getparamiter(strWebEdit,StrStatus,Myfile)
'
'	On error resume next
'	iRowCount = Datatable.getSheet("TestStep").getRowCount
' 	Datatable.getSheet("TestStep").setCurrentRow(iRowCount+1)
' 	DataTable.Value("No","TestStep") = strWebEdit
' 	DataTable.Value("LogStep","TestStep") = StrStatus
' 	DataTable.Value("CapScreen","TestStep") = Myfile
' 	DataTable.Value("StartTime","TestStep") = StartTime
' 	DataTable.Value("EndTime","TestStep") = EndTime
'End Sub  


Public Sub StartTime(StrStartTime)

StrStartTime = Time()
'MsgBox startTime
End Sub  

Public Sub EndTime(StrEndTime)

StrEndTime = Time()
'MsgBox startTime
End Sub  

Public Function Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)

	On error resume next
	iRowCount = Datatable.getSheet("TestStep").getRowCount
 	Datatable.getSheet("TestStep").setCurrentRow(iRowCount+1)
 	DataTable.Value("No","TestStep") = strWebEdit
 	DataTable.Value("LogStep","TestStep") = StrStatus
 	DataTable.Value("CapScreen","TestStep") = Myfile
 	DataTable.Value("StartTime","TestStep") = StrStartTime
 	DataTable.Value("EndTime","TestStep") = StrEndTime
 	
End Function  

Public Sub GettextOnedit(obj_WpfWindow,strWebEdit)



	On error resume next
	Call StartTime(StrStartTime)
	If obj_WpfWindow <> "" Then	
	'WpfWindow("Micro Focus MyFlight Sample").WpfObject("John Smith").GetROProperty("text")


		With WpfWindow(obj_WpfWindow)			
   			If .WpfObject(strWebEdit).Exist(0) Then
   				strWebEdit = WpfWindow(obj_WpfWindow).WpfObject(strWebEdit).GetROProperty("text")
   			   
  				Call EndTime(StrEndTime)
			   '	If err.number = 0 Then
        			PrintInfo "micPass", strWebEdit,  " Error Description: " & Err.Description
        			StrStatus = strWebEdit & "Step is Pass"
        			Call CaptureScreenShot(strFileName)
        			Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime)
        			Err.clear						
					Exit sub
				'End If
   			Else
   			    PrintInfo "micFail", strWebEdit, strWebEdit & "WebEdit not found."   
   			    StrStatus = strWebEdit & "Step is False"
   			    Call EndTime(StrEndTime)
   			    Call CaptureScreenShot(strFileName)
				Call Getparamiter(strWebEdit,StrStatus,Myfile,StrStartTime,StrEndTime) 			   
         	End If         	
		End With			
	End If
End Sub 



