﻿
memName = DataTable("Firstname",dtglobalsheet)
lastName = DataTable("Lastname",dtglobalsheet)
dataRow = Parameter("dataRow")

RunAction "MemberLogin", oneIteration, dataRow

tmp_timeout = 0
Do until Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,Login page loading for long","")
		Call Fnc_ExitIteration("Timeout,Login page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do

	End If
	tmp_timeout = tmp_timeout + 1
Loop
Call WriteLog("Login page loading completed","")
CaptureScreenShot("")


If Browser("Browser").Page("Page").WebButton("toolbar_walkin").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("toolbar_walkin").Click
End If

Wait 0.1

If Browser("Browser").Page("Page").WebEdit("memName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("memName").Click
	Wait 0.1
	Browser("Browser").Page("Page").WebEdit("memName").Set memName
	Wait 0.1
	Browser("Browser").Page("Page").WebEdit("memLastname").Click
	Wait 0.1
	Browser("Browser").Page("Page").WebEdit("memLastname").Set lastName
	Wait 0.1
	CaptureScreenShot("")
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
	Wait 0.5
End If

Wait 20

'3.1 (ตามขั้นตอนการแลกของกำนัล LOY_PR_006)

If Browser("Browser").Page("Page").WebElement("WebTable01").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("WebTable01").DoubleClick	
End If
Wait 0.1

If Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Exist(5) Then
	Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Click
	Wait 0.1
	If Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(5)	Then
			Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
			Wait 0.1
			CaptureScreenShot("")
			If Browser("Browser").Page("Page").WebButton("buttonสร้าง").Exist(10) Then
				Browser("Browser").Page("Page").WebButton("buttonสร้าง").Click
			End If
			

	End If
End If

Wait 10
If Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebElement("ติดต่อด้วยตนเอง").Click
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebElement("รถเข็น").Click
End If

Wait 3
'Shopping Page Popup
	
If Browser("Browser").Page("Page").WebElement("Shopping Page Popup").Exist(10) Then
	Browser("Browser").Page("Page").WebEdit("search_product_name").Click
	Wait 1
	Browser("Browser").Page("Page").WebEdit("search_product_name").Set "Gift Voucher Central 500 บาท"
	CaptureScreenShot("")
End If

Wait 1

If Browser("Browser").Page("Page").WebElement("เลือกประเภท").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("เลือกประเภท").Click
		Wait 1
		Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Click
		Wait 1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("ค้นหา").Click				
		Wait 0.1
		CaptureScreenShot("")
		Wait 1
		Browser("Browser").Page("Page").WebElement("รถเข็นแดง").Click
End If

Wait 0.5

If Browser("Browser").Page("Page").WebElement("เลือกของกำนัล").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("เลือกของกำนัล").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebEdit("quantity").Set "1"
		wait 0.1
		CaptureScreenShot("")
		If Browser("Browser").Page("Page").WebElement("เลือกวิธีส่งมอบ").Exist(5) Then
				Browser("Browser").Page("Page").WebElement("เลือกวิธีส่งมอบ").Click
				Wait 0.3
				Browser("Browser").Page("Page").WebElement("รับด้วยตนเอง").Click
				Wait 0.3
				Browser("Browser").Page("Page").WebElement("ค้นหาที่อยู่").Click
			
				If Browser("Browser").Page("Page").WebElement("ค้นหาศูนย์บริการลูกค้า").Exist(10) Then
						Browser("Browser").Page("Page").WebElement("ค้นหาศูนย์บริการลูกค้า").Click
						Wait 0.1
						Browser("Browser").Page("Page").WebEdit("cscServiceCenterName").Set "ส่วนลูกค้าสัมพันธ์ ชั้น 6 (สำนักงานใหญ่)"
						CaptureScreenShot("")
						Wait 0.1
						Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
						Wait 3
						CaptureScreenShot("")
						Browser("Browser").Page("Page").WebElement("เลือกส่วนลูกค้าสัมพันธ์ ชั้น 6 (สำนักงานใหญ่)").DoubleClick
						Wait 0.1
						If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
								Browser("Browser").Page("Page").WebButton("ตกลง").Click
						End If
						
				End If
				
		End If
		
End If

Wait 0.5

If Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Exist Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Click
End If

Wait 0.5

If Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Exist(10) Then
		Wait 0.5
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Click
		Wait 0.1
		If Browser("Browser").Page("Page").WebElement("ต้องการยืนยันหรือไม่").Exist(10) Then
				If Browser("Browser").Page("Page").WebButton("ใช่").Exist(5) Then
						Wait 0.1
						CaptureScreenShot("")
						Browser("Browser").Page("Page").WebButton("ใช่").Click
						Wait 0.1
				End If
					
		End If
End If
Wait 0.5

If Browser("Browser").Page("Page").WebElement("ยืนยันการแลกของกำนัลเรียบร้อยแ").Exist(10) Then
		mg = Browser("Browser").Page("Page").WebElement("ยืนยันการแลกของกำนัลเรียบร้อยแ").GetROProperty("innertext")
		Call WriteLog(""&mg,"")
		Wait 0.1
		
End If
Wait 0.5

If Browser("Browser").Page("Page").WebButton("ตกลง_2").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("ตกลง_2").Click
		Call WriteLog("สามารถทำการแลกของกำนัลได้","")

End If

Wait 3

If Browser("Browser").Page("Page").Link("เรื่องที่ให้บริการ").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").Link("เรื่องที่ให้บริการ").Click
End If
Wait 0.5


'4 ระบบเปิดหน้าเรื่องที่ให้บริการพร้อมสร้างเรื่องที่ให้บริการใหม่
Call WriteLog("ระบบเปิดหน้าเรื่องที่ให้บริการพร้อมสร้างเรื่องที่ให้บริการใหม่","")

numberNo = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,2)
Call WriteLog("เลขที่รับเรื่อง : "&numberNo,"")

nameCostomer = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,3)
Call WriteLog("ชื่อลูกค้า : "&nameCostomer,"")

redeemTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,4)
Call WriteLog("ประเภทเรื่อง : "&redeemTyp,"")

subredeemTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,5)
Call WriteLog("ประเภทเรื่องย่อย : "&subredeemTyp,"")

status  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,6)
Call WriteLog("สถานะ : "&status,"")

responSile   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,7)
Call WriteLog("ผู้รับผิดชอบ : "&responSile,"")

dateTime   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,8)
Call WriteLog("วันที่รับเรื่อง : "&dateTime,"")



'5 บันทึกผลการติดต่อ


If Browser("Browser").Page("Page").WebElement("ประวัติการติดต่อ").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("ประวัติการติดต่อ").Click
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").Link("สรุปการติดต่อ").Click
End If

'tap แลกของกำนัล
Call WriteLog("Get Data แท็บแลกของกำนัล","")

numberNo = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,2)
Call WriteLog("เลขที่รายการแลกของกำนัล : "&numberNo,"")

nameRedeem = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,3)
Call WriteLog("ของกำนัลที่ต้องการแลก : "&nameRedeem,"")

redeemNum  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,4)
Call WriteLog("จำนวน : "&redeemNum,"")

Q1  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,5)
Call WriteLog("เงื่อนไขการชำระ : "&Q1,"")

service  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,6)
Call WriteLog("วิธีการส่งมอบ : "&service,"")

status   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,7)
Call WriteLog("สถานะ : "&status,"")

serviceBy   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,8)
Call WriteLog("ผู้รับผิดชอบ : "&serviceBy,"")

dateTime   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,9)
Call WriteLog("วันที่รับเรื่อง : "&dateTime,"")
Wait 0.1
CaptureScreenShot("")


'tap  เรื่องให้บริการ

Call WriteLog("Get Data แท็บเรื่องให้บริการ","")
Wait 0.1
If Browser("Browser").Page("Page").Link("เรื่องให้บริการ").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").Link("เรื่องให้บริการ").Click
End If


numberNo = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,2)
Call WriteLog("เลขที่รับเรื่อง : "&numberNo,"")

nameCostomer = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,3)
Call WriteLog("ชื่อลูกค้า : "&nameCostomer,"")

nameTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,4)
Call WriteLog("ประเภทเรื่อง : "&nameTyp,"")

subnameTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,5)
Call WriteLog("ประเภทเรื่องย่อย : "&subnameTyp,"")

deGree  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,6)
Call WriteLog("ระดับความสำคัญ : "&deGree,"")

maxDegree   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,7)
Call WriteLog("ระดับความรุนแรง : "&maxDegree,"")

status   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,8)
Call WriteLog("สถานะ : "&status,"")

serviceBy   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,9)
Call WriteLog("ผู้รับผิดชอบ : "&serviceBy,"")

dateTime   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,10)
Call WriteLog("วันที่รับเรื่อง : "&dateTime,"")


Wait 0.1

If Browser("Browser").Page("Page").WebButton("toolbar_walkinout").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("toolbar_walkinout").Click
		Call WriteLog("สุดการติดต่อ","")
End If









