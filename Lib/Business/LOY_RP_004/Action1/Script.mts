﻿
'RunAction "RedeemActivity [RedeemActivity]", oneIteration, DTRow
RunAction "MemberLogin [Loyalty]", oneIteration, DTRow
memName = DataTable("Firstname",dtglobalsheet)
dataRow = Parameter("dataRow")


tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebElement("แลกของกำนัล").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,Login page loading for long","")
		Call Fnc_ExitIteration("Timeout,Login page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	Else 	
		Browser("Browser").Page("Page").WebElement("แลกของกำนัล").Click
	End If
	tmp_timeout = tmp_timeout + 1
Loop
Call WriteLog("Login page loading completed","")
CaptureScreenShot("") @@ hightlight id_;_Browser("Browser").Page("Page").Link("จัดการของกำนัล")_;_script infofile_;_ZIP::ssf3.xml_;_

If Browser("Browser").Page("Page").Link("จัดการของกำนัล").Exist(10) Then
		CaptureScreenShot("")
		Wait 0.1
		Browser("Browser").Page("Page").Link("จัดการของกำนัล").Click
	
End If

Wait 0.5

If Browser("Browser").Page("Page").WebEdit("searchProductName").Exist(5) Then
		Browser("Browser").Page("Page").WebEdit("searchProductName").Set "ทดสอบกิจกรรม1"
End If

Wait 0.5

If Browser("Browser").Page("Page").WebElement("WebElement").Exist(10) Then
	Browser("Browser").Page("Page").WebElement("WebElement").Click
End If

Wait 0.5

If Browser("Browser").Page("Page").WebElement("กิจกรรม").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("กิจกรรม").Click
End If
Wait 0.4

If Browser("Browser").Page("Page").WebButton("ค้นหา").Exist(10) Then
		Browser("Browser").Page("Page").WebButton("ค้นหา").Click
End If
CaptureScreenShot("")


Wait 0.5

If Browser("Browser").Page("Page").WebElement("WebTable").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("WebTable").DoubleClick
End If

Wait 0.4

If Browser("Browser").Page("Page").Link("Check-In").Exist(10) Then
		Browser("Browser").Page("Page").Link("Check-In").Click
End If

If Browser("Browser").Page("Page").WebElement("WebElement_2").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("WebElement_2").Click
End If

If Browser("Browser").Page("Page").WebElement("รอร่วมกิจกรรม").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("รอร่วมกิจกรรม").Click
End If
Wait 0.1 @@ hightlight id_;_1988164328_;_script infofile_;_ZIP::ssf25.xml_;_


'==================================Start Data Export File ===============================
Call WriteLog("Start Data Export File","")	
CaptureScreenShot("")
If Browser("Browser").Page("Page").WebButton("Export").Exist(10) Then
		Browser("Browser").Page("Page").WebButton("Export").Click
End If

Wait 0.5

If Browser("Browser").WinObject("Notification").WinToolbar("Notification").Exist(10) Then
		If Browser("Browser").WinObject("Notification").WinButton("6").Exist(10) Then
				Wait 1
				Browser("Browser").WinObject("Notification").WinButton("6").Click
				Wait 1
				CaptureScreenShot("")
				Browser("Browser").WinMenu("ContextMenu").Select "Save as"
		End If
		CaptureScreenShot("")
		Wait 1
		'Browser("Browser").Dialog("Save As").WinEdit("WinEdit").Click 230, 19, micRightBtn @@ hightlight id_;_199928_;_script infofile_;_ZIP::ssf26.xml_;_
		

		Wait 1
		'Browser("Browser").Dialog("Save As").WinEdit("WinEdit").WinMenu("ContextMenu").Select "Paste"
		Wait 1
		Browser("Browser").Dialog("Save As").WinEdit("WinEdit_excel").Type "D:\Loyalty\Data\CheckIn_Test1.xlsx"
		CaptureScreenShot("")
		
		Wait 5
		Browser("Browser").Dialog("Save As").WinButton("SaveExcel").Highlight
		Browser("Browser").Dialog("Save As").WinButton("SaveExcel").Click
		

	


End If

Wait 0.5
'Browser("Browser").WinObject("Notification").WinButton("Open folder").Click @@ hightlight id_;_2057200472_;_script infofile_;_ZIP::ssf36.xml_;_
'Wait 0.1
'Browser("Browser").WinObject("Notification").WinButton("Open_2").Click
'Wait 0.1
''Window("Excel").WinObject("Message Bars").WinButton("Enable Editing").Click
'Wait 0.1
'Window("Excel").WinObject("Ribbon").WinButton("Close").Click
'

'
' Set fso = CreateObject("Excel.Application")
' Set objExcel = fso        
' Set objWorkBook = objExcel.Workbooks.Open ("D:\Loyalty\Data\CheckIn_Test1.xlsx")
' Set objWorkSheet = objWorkBook.WorkSheets("sheet1")
'
' 
' objWorkbook.SaveAs "D:\Loyalty\Data\CheckIn_Test2.xlsx"
'
' objworkbook.Close  
'    objExcel.DisplayAlerts = False
' objExcel.Quit    
'
' Set objWorkSheet = Nothing
' Set objWorkbook = Nothing 
' Set objExcel = nothing
'
'
''===================================Start Modify ImportSheet Data ===============================
CaptureScreenShot("")
Call WriteLog("Start Modify ImportSheet Data","")	
Wait 10
'On Error Resume Next
DataTable.AddSheet("LOY_RP_004")
DataTable.ImportSheet "D:\Loyalty\Data\CheckIn_Test1.xlsx","sheet1","LOY_RP_004"


'DataRow = 1 To DataTable.LocalSheet.GetRowCount
For Row = 1 To DataTable.GetSheet("LOY_RP_004").GetRowCount Step 1
	DataTable.GetSheet("LOY_RP_004").SetCurrentRow(Row)
'	DataTable.LocalSheet.SetCurrentRow(Row)
	DataTable.Value("ผลการเข้าร่วมกิจกรรม","LOY_RP_004") = "Y"
	DataTable.Value("จำนวนที่_CheckIn","LOY_RP_004") = "1"
	
	Next
	
Wait 0.1
CaptureScreenShot("")
Call WriteLog("Start  ExportSheet Modify Data Passed","")	
DataTable.ExportSheet "D:\Loyalty\Data\CheckIn_Test2.xlsx","LOY_RP_004"

Wait 0.1

'===================================Start  ImportSheet Update Data ===============================
CaptureScreenShot("")
Call WriteLog("Start  ImportSheet Update Data","")
If Browser("Browser").Page("Page").WebButton("Import").Exist(10) Then
		Browser("Browser").Page("Page").WebButton("Import").Click
		Wait 0.7
		Browser("Browser").Page("Page").WebFile("file").Set "D:\Loyalty\Data\CheckIn_Test2.xlsx"
		
		CaptureScreenShot("")
		
		If Browser("Browser").Page("Page").WebButton("อัพโหลด").Exist(10) Then
				Browser("Browser").Page("Page").WebButton("อัพโหลด").Click
				Wait 1
		End If
		
		If Browser("Browser").Page("Page").WebButton("นำเข้า").Exist(10) Then
				Browser("Browser").Page("Page").WebButton("นำเข้า").Click
		End If
		Wait 3
		
		If Browser("Browser").Page("Page").WebElement("นำเข้าไฟล์สำเร็จ").Exist(10) Then
			mg = Browser("Browser").Page("Page").WebElement("นำเข้าไฟล์สำเร็จ").GetROProperty("innertext")
			Call WriteLog(" "&mg,"")
						
			CaptureScreenShot("")
		End If
		
		Wait 0.7
		If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(10) Then
				Browser("Browser").Page("Page").WebButton("ตกลง").Click
		End If		
End If
Wait 0.7 @@ hightlight id_;_Browser("Browser").Page("Page").WebButton("Import")_;_script infofile_;_ZIP::ssf29.xml_;_
'ค้นหาและตรวจสอบผลการปรับปรุงรายการ @@ hightlight id_;_Browser("Browser").Page("Page").WebFile("file")_;_script infofile_;_ZIP::ssf30.xml_;_
If Browser("Browser").Page("Page").WebEdit("memberFname").Exist(15) Then
		Browser("Browser").Page("Page").WebEdit("memberFname").Set memName
		Wait 0.1
		Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
End If

fName = Browser("Browser").Page("Page").WebElement("Table1").GetROProperty("innertext")

status = Browser("Browser").Page("Page").WebElement("จัดกิจกรรมเรียบร้อยแล้ว").GetROProperty("innertext")

Wait 0.7
If Trim (memName) = Trim(fName) Then
	If Trim (status) = "จัดกิจกรรมเรียบร้อยแล้ว" Then
			Call WriteLog(" "&fName &status,"")
			Wait 0.1
			CaptureScreenShot("")
	End If
	
End If
 @@ hightlight id_;_5374120_;_script infofile_;_ZIP::ssf39.xml_;_
