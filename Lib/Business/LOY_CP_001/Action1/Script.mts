﻿'============================== Start data table to variable ==============================
dataRow = Parameter("DTRow")
'============================== End data table to variable ================================


'============================== Start Login Application ===================================

RunAction "MemberLogin", oneIteration, dataRow

'============================== End Login Application =====================================





'============================== Start เจ้าหน้าที่ทีม CRM นำเข้าข้อมูลกลุ่มเป้าหมาย =====================
If Browser("Browser").Page("Page").WebElement("แคมเปญ").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("แคมเปญ").Click
		Wait 0.1
		Browser("Browser").Page("Page").Link("กลุ่มเป้าหมาย").Click
		Wait 0.1
		If Browser("Browser").Page("Page").WebButton("นำข้อมูลเข้า").Exist(5) Then
				Browser("Browser").Page("Page").WebButton("นำข้อมูลเข้า").Click
				Wait 0.1
				Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
				Wait 0.1
				Browser("Browser").Page("Page").WebFile("file").Set "D:\Loyalty\Data\FileUpload\Test-Target-Campaign.xlsx" @@ hightlight id_;_Browser("Browser").Page("Page").WebFile("file")_;_script infofile_;_ZIP::ssf2.xml_;_
				Wait 0.1
				Browser("Browser").Page("Page").WebEdit("name").Set "TargetList1"
				wait 3
				Browser("Browser").Page("Page").WebButton("โหลดไฟล์").Click
				wait 3
				Call WriteLog("Click Search Customer/Member Completed","")	
				Wait 0.1
				CaptureScreenShot("")
				wait 1
				If Browser("Browser").Page("Page").WebButton("นำเข้า").Exist(5) Then
						Browser("Browser").Page("Page").WebButton("นำเข้า").Click
						If Browser("Browser").Page("Page").WebElement("นำเข้าข้อมูลเรียบร้อยแล้ว").Exist(5) Then
								Browser("Browser").Page("Page").WebElement("นำเข้าข้อมูลเรียบร้อยแล้ว").Click
								mg = Browser("Browser").Page("Page").WebElement("นำเข้าข้อมูลเรียบร้อยแล้ว").GetROProperty("innertext")
								Call WriteLog(""&mg,"")
								Wait 0.1
								CaptureScreenShot("")
								Browser("Browser").Page("Page").WebButton("ตกลง").Click
						End If
				End If	
				
		End If		
End If


Wait 0.1
Browser("Browser").Page("Page").WebEdit("startDt").set "15022562"
'Browser("Browser").Page("Page").WebEdit("startDt").Type micTab
Wait 1
Browser("Browser").Page("Page").WebEdit("endDt").Click

'Browser("Browser").Page("Page").WebButton("startDt").Click
Wait 3
Browser("Browser").Page("Page").WebEdit("endDt").set "31032562"
Wait 1

 @@ hightlight id_;_Browser("Browser 2").Page("Page").WebButton("WebButton")_;_script infofile_;_ZIP::ssf6.xml_;_
 @@ hightlight id_;_Browser("Browser 2").Page("Page").WebEdit("endDt")_;_script infofile_;_ZIP::ssf7.xml_;_


'Browser("Browser").Page("Page").WebElement("กลุ่มเป้าหมาย").Click

If Browser("Browser").Page("Page").WebElement("แหล่งที่มา").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("แหล่งที่มา").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebElement("Internal Source").Click
End If
Wait 5
Browser("Browser").Page("Page").WebElement("สถานะ").Click
Wait 5
Browser("Browser").Page("Page").WebElement("ใช้งาน").Click
Wait 5
Browser("Browser").Page("Page").WebButton("ค้นหา").Click
Wait 2
Browser("Browser").Page("Page").WebElement("campaignTargetGroup_table").DoubleClick

'============================== End เจ้าหน้าที่ทีม CRM นำเข้าข้อมูลกลุ่มเป้าหมาย =======================


'============================= เจ้าหน้าที่ทีม CRM ตรวจสอบข้อมูลกลุ่มเป้าหมายที่นำเข้า =====================


'================ เจ้าหน้าที่ทีม CRM สร้างข้อมูลแคมเปญในส่วน แคมเปญประเภท “Acquisition” ===============
Wait 3
If Browser("Browser").Page("Page").WebElement("แคมเปญ").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("แคมเปญ").Click
	Wait 0.3
	If Browser("Browser").Page("Page").Link("ข้อมูลแคมเปญ").Exist(3) Then
		Browser("Browser").Page("Page").Link("ข้อมูลแคมเปญ").Click
		Wait 0.3
		Browser("Browser").Page("Page").WebButton("สร้างแคมเปญ").Click
			Wait 1
			Browser("Browser").Page("Page").WebEdit("campaignName").Click
			Wait 0.3
			Browser("Browser").Page("Page").WebEdit("campaignName").Set "แคมเปญ1"
			Wait 0.1
			Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebElement("Acquisition").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("plannedStartDtTh").Set "15022562"
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("plannedEndDtTh").Set "31032562"
			Wait 0.1
			actualStatusWaiting = Browser("Browser").Page("Page").WebElement("รอใช้งาน").GetROProperty("innertext")
			Wait 0.1
			Browser("Browser").Page("Page").WebElement("ค้นหาผู้รับผิดชอบ").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Set "วาสนา แก้วกงพาน"
			Wait 0.1
			Browser("Browser").Page("Page").WebButton("ค้นหาsearchEmployName").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebElement("WebTablesearchEmployName").DoubleClick		
	End If
End If

If strComp(actualStatusWaiting,"รอใช้งาน") = 0 Then
	Else
	Call Fnc_ExitIteration("actualStatusWaiting","","","")
	CaptureScreenShot("")
End If
'Wait 0.2
'If Browser("Browser").Page("Page").Link("กดออกข้อมูลแคมเปญ×").Exist(5) Then
'		Browser("Browser").Page("Page").WebButton("×").Click
'End If

Wait 0.1
'actualResponsible = Browser("Browser").Page("Page").WebEdit("ownerName").GetROProperty("innertext")
'Call WriteLog("ผู้รับผิดชอบ" & " "& actualResponsibleactualResponsible,"")	
If Browser("Browser").Page("Page").WebEdit("description").Exist(3) Then
	Browser("Browser").Page("Page").WebEdit("description").Set "แคมเปญสำหรับหาสมาชิกใหม่"
	If Browser("Browser").Page("Page").WebElement("จุดประสงค์").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("จุดประสงค์").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebElement("แจ้งข้อมูล/ข่าวสาร").Click
	End If
End If

wait 2
If Browser("Browser").Page("Page").WebButton("บันทึก").Exist(3) Then
	Browser("Browser").Page("Page").WebButton("บันทึก").Click
End If
wait 1
actualCampaignCode = Browser("Browser").Page("Page").WebEdit("campaignCode").GetROProperty("innertext")
CaptureScreenShot("")

'5'===================== เจ้าหน้าที่ทีม CRM เลือกข้อมูลจากกลุ่มเป้าหมาย มาผูกภายใต้แคมเปญ ===================
wait 1
If Browser("Browser").Page("Page").Link("กลุ่มเป้าหมาย_2").Exist(3) Then
	Browser("Browser").Page("Page").Link("กลุ่มเป้าหมาย_2").Click
	Browser("Browser").Page("Page").Link("รายการกลุ่มเป้าหมาย").Click
End If

If Browser("Browser").Page("Page").WebButton("สร้าง_2").Exist(3) Then
	Browser("Browser").Page("Page").WebButton("สร้าง_2").Click
	If Browser("Browser").Page("Page").WebElement("campaign_campaignTargetTabDetail_btnTargetName").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("campaign_campaignTargetTabDetail_btnTargetName").Click
		If Browser("Browser").Page("Page").WebEdit("targetName").Exist(3) Then
			Browser("Browser").Page("Page").WebEdit("targetName").Set "TargetList1"
			Wait 0.1
			If Browser("Browser").Page("Page").WebButton("ค้นหา_2").Exist(5) Then
					Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
					Wait 2
					CaptureScreenShot("")
					
					Browser("Browser").Page("Page").WebElement("searchCampaignTargetListModal_table").DoubleClick
			End If
			Wait 0.1
			Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
			Wait 0.1
			
			'Upload เอกสารแนบ
			If Browser("Browser").Page("Page").Link("เอกสารแนบ").Exist(3) Then
					Browser("Browser").Page("Page").Link("เอกสารแนบ").Click
					Wait 0.1 
					Browser("Browser").Page("Page").WebButton("เพิ่ม_2").Click
					wait 2
					Browser("Browser").Page("Page").WebElement("upload").Click
					Wait 1
					Browser("Browser").Page("Page").WebFile("file_3").Set "D:\Loyalty\Data\CampaignDocument.pdf"
					'Browser("Browser").Page("Page").WebFile("file").Set "D:\Loyalty\Data\CampaignDocument.pdf"
					Wait 3
					Browser("Browser").Page("Page").WebButton("อัพโหลด").Click
					If Browser("Browser").Page("Page").WebElement("ไฟล์ : เอกสารแนบCampaignทีแอลแ").Exist(5) Then
							Browser("Browser").Page("Page").WebButton("ตกลงupload").Click
							Wait 0.1
							Browser("Browser").Page("Page").WebButton("บันทึกupload").Click
							Wait 0.1
							Browser("Browser").Page("Page").WebElement("บันทึกรายการเอกสารแนบเรียบร้อย").Click
							Wait 0.1
							Browser("Browser").Page("Page").WebButton("ตกลงupload").Click
							
					End If	
			End If
		End If
	End If
End If


'============== เจ้าหน้าที่ทีม CRM แก้ไขสถานะและรายละเอียดของกลุ่มเป้าหมาย ภายใต้แคมเปญ ===================


'6'===================== เจ้าหน้าที่ทีม CRM สร้างข้อมูลการจัดการช่องทาง (โทรออก) =========================
Wait 2

If Browser("Browser").Page("Page").Link("จัดการช่องทาง").Exist(5) Then
		Browser("Browser").Page("Page").Link("จัดการช่องทาง").Click
		Wait 0.3
		If  Browser("Browser").Page("Page").WebButton("สร้าง_3").Exist(5) then
				Browser("Browser").Page("Page").WebButton("สร้าง_3").Click
				Wait 0.3
				Browser("Browser").Page("Page").WebElement("campaign_campaignChannelTabDetail_form").Click
				Wait 0.1
				Browser("Browser").Page("Page").WebElement("โทรออก").Click
				Wait 0.1
				If Browser("Browser").Page("Page").WebElement("Taptarget1").Exist(5) Then
						Browser("Browser").Page("Page").WebElement("Taptarget1").Click
						Wait 0.1
						Browser("Browser").Page("Page").WebElement("TargetList1").Click
				End If
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("activationDt").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("activationDt").Set  "15022562"
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("expirationDt").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("expirationDt").Set   "31032562"
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("description").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebEdit("description").Set "ทดสอบแคมเปญ"
			Wait 0.1
			Browser("Browser").Page("Page").WebElement("WebElement_2").Click
			Wait 0.1
			Browser("Browser").Page("Page").WebElement("ใช้งาน").Click
			Wait 0.1
			CaptureScreenShot("")
			If Browser("Browser").Page("Page").WebButton("บันทึก").Exist(5) Then
					Browser("Browser").Page("Page").WebButton("บันทึก").Click
					If Browser("Browser").Page("Page").WebElement("บันทึกเสร็จสมบูรณ์").Exist(5) Then
							ms = Browser("Browser").Page("Page").WebElement("บันทึกเสร็จสมบูรณ์").GetROProperty("innertext")
							CaptureScreenShot("")
							Call WriteLog(""&ms,"")	
							Wait 0.1
							Browser("Browser").Page("Page").WebButton("ตกลงบันทึกเสร็จสมบูรณ์").Click
							Wait 0.1
							If Browser("Browser").Page("Page").WebElement("โหลดรายชื่อลูกค้าภายใต้ช่องทาง").Exist(5) Then
									mh = Browser("Browser").Page("Page").WebElement("โหลดรายชื่อลูกค้าภายใต้ช่องทาง").GetROProperty("innertext")
									CaptureScreenShot("")
							        Call WriteLog(""&mh,"")	
							        Wait 0.1
							        Browser("Browser").Page("Page").WebButton("ตกลงโหลดรายชื่อลูกค้าภายใต้ช่องทาง").Click
							End If

					End If					
			End If
				
		End If
		
End If


'7'====================== ผู้จัดการทีม CRM ค้นหาข้อมูลแคมเปญที่ต้องการจะประกาศใช้งาน =====================

If Browser("Browser").Page("Page").WebElement("แคมเปญ").Exist(5) Then
		Wait 0.1
		Browser("Browser").Page("Page").Link("ข้อมูลแคมเปญ").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebElement("ค้นหาแคมเปญ").Click
		If Browser("Browser").Page("Page").WebElement("รอใช้งาน").Exist(5) Then
				Wait 0.1
				CaptureScreenShot("")
				mg = Browser("Browser").Page("Page").WebElement("รอใช้งาน").GetROProperty("innertext")
				Call WriteLog(""&mg,"")	
				Wait 0.1
					If Browser("Browser").Page("Page").WebButton("ค้นหาแคมเปญ").Click Then
							Browser("Browser").Page("Page").WebButton("ค้นหาแคมเปญ").Click
							CaptureScreenShot("")
							Wait 0.1
							Browser("Browser").Page("Page").WebElement("แคมเปญ1").DoubleClick
					End If
		End If
		
End If


'8'====================== ผู้จัดการทีม CRM ตรวจสอบข้อมูลแคมเปญที่ต้องการจะประกาศใช้งาน ===================
If Browser("Browser").Page("Page").Link("เอกสารแนบ").Click Then
		CaptureScreenShot("")
		Call WriteLog("ผู้จัดการทีม CRM ตรวจสอบข้อมูลแคมเปญที่ต้องการจะประกาศใช้งาน","")
End If
Wait 0.1



'9'================================ผู้จัดการทีม CRM ประกาศใช้งานแคมเปญ =============================
If Browser("Browser").Page("Page").Link("รายละเอียด").Exist(5) Then
		Browser("Browser").Page("Page").Link("รายละเอียด").Click
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("ประกาศใช้งาน").Click
		Call WriteLog("ผู้จัดการทีม CRM ประกาศใช้งานแคมเปญ ","")
End If


'========================== หัวหน้าทีมอาวุโสทีม TLC Call Center ค้นหาข้อมูลแคมเปญ ==================


'=======หัวหน้าทีมอาวุโสทีม TLC Call Center ตรวจสอบการมอบหมายรายชื่อกลุ่มเป้าหมายภายใต้แคมเปญ (มอบให้) ====


'========= หัวหน้าทีมอาวุโสทีม TLC Call Center มอบหมายรายชื่อกลุ่มเป้าหมายภายใต้แคมเปญ (มอบให้) ===========


'======================= หัวหน้าทีม  TLC Call Center  (ทีม 1) ค้นหาข้อมูลแคมเปญ ==================

 @@ hightlight id_;_Browser("Browser").Page("Page").WebFile("file 4")_;_script infofile_;_ZIP::ssf10.xml_;_
 @@ hightlight id_;_Browser("Browser").Page("Page").WebElement("WebElement 5")_;_script infofile_;_ZIP::ssf11.xml_;_
 @@ hightlight id_;_Browser("Browser").Page("Page").WebFile("file 3")_;_script infofile_;_ZIP::ssf12.xml_;_
