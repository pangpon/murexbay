﻿'============================== Start data table to variable ==============================
strRedemptionProduct = "Redemption Product"
strRedemptionStatus = "อยู่ระหว่างดำเนินการ"
strAmtPerUnit = "1"
strLevel = "3 - Level 3"
strPromoName = "แลกของกำนัลทั่วไป"

strProductName = DataTable("ProductName",dtglobalsheet)
strCcMproductName = DataTable("CcMproductName",dtglobalsheet)
strEmployName = DataTable("EmployName",dtglobalsheet)
strStartDtDisplay = DataTable("StartDtDisplay",dtglobalsheet)
strEndDtDisplay = DataTable("EndDtDisplay",dtglobalsheet)
strWorkItemCd = DataTable("WorkItemCd",dtglobalsheet)	
strWorkItemName = DataTable("WorkItemName",dtglobalsheet)
strWorkgroupCd = DataTable("WorkgroupCd",dtglobalsheet)
strWorkgroupName = DataTable("WorkgroupName",dtglobalsheet)
strMobileChannelFlg = DataTable("MobileChannelFlg",dtglobalsheet)
strSelfReceiveFlg = DataTable("SelfReceiveFlg",dtglobalsheet)
strPostReceiveFlg = DataTable("PostReceiveFlg",dtglobalsheet)
strMshortDetail = DataTable("MshortDetail",dtglobalsheet)
strMdetai = DataTable("Mdetail",dtglobalsheet)
strProgramName = DataTable("ProgramName",dtglobalsheet)
strPointTypeName = DataTable("PointTypeName",dtglobalsheet)
strPoint = DataTable("Point",dtglobalsheet)
strPartnerPoint = DataTable("PartnerPoint",dtglobalsheet)
strConditionDescription = DataTable("ConditionDescription",dtglobalsheet)
strQuantity = DataTable("Quantity",dtglobalsheet)
strOriginalQty	= DataTable("OriginalQty",dtglobalsheet)
strCommentQty = DataTable("CommentQty",dtglobalsheet)
strServiceCenterName = DataTable("ServiceCenterName",dtglobalsheet)
strQuota = DataTable("Quota",dtglobalsheet)
strMobilePhone = DataTable("MobilePhone",dtglobalsheet)


dataRow = Parameter("DTRow")
'============================== End data table to variable =================================


'============================== Start Login Application ====================================

RunAction "MemberLogin", oneIteration, dataRow

'============================== End Login Application ====================================== @@ hightlight id_;_Browser("Browser").Page("Page").Link("การแลกของกำนัล")_;_script infofile_;_ZIP::ssf3.xml_;_
 

'=========================== Start เจ้าหน้าที่ CRM สร้างข้อมูลของกำนัล =============================== @@ hightlight id_;_Browser("Browser").Page("Page").WebElement("WebElement")_;_script infofile_;_ZIP::ssf7.xml_;_

tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebElement("รายการ Activity").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,Home My Task page loading for long","")
		Call Fnc_ExitIteration("Timeout,Home My Task page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
Call WriteLog("Loading Home My Task Completed","")
CaptureScreenShot("")

If Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(3) Then
	Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
	If Browser("Browser").Page("Page").Link("จัดการของกำนัล").Exist(3) Then
		Browser("Browser").Page("Page").Link("จัดการของกำนัล").Click
		tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Exist(5) = True
				Wait 1
				If tmp_timeout = 10 Then
					
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
		If Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Exist(3) Then		
		Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Click
		End If
	End If
End If

tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebElement("Redemption Product").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,จ้าหน้าที่ CRM สร้างข้อมูลของกำนัล page loading for long","")
		Call Fnc_ExitIteration("Timeout,จ้าหน้าที่ CRM สร้างข้อมูลของกำนัล page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
If Browser("Browser").Page("Page").WebElement("Redemption Product").Exist(5) Then
	actualRedemptionProduct = Browser("Browser").Page("Page").WebElement("Redemption Product").GetROProperty("innertext")
	actualRedemptionStatus = Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").GetROProperty("innertext")
	actualAmtPerUnit = Browser("Browser").Page("Page").WebEdit("ccAmtPerUnit").GetROProperty("value")
	actualLevel = Browser("Browser").Page("Page").WebElement("3 - Level 3").GetROProperty("innertext")
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : จ้าหน้าที่ CRM สร้างข้อมูลของกำนัล")
End If

If strComp(actualRedemptionProduct,strRedemptionProduct) = 0 Then
	Else 
	Call Fnc_ExitIteration(RedemptionProductStatus)
	If strComp(actualRedemptionStatus,strRedemptionStatus) = 0 Then
		Else
		Call Fnc_ExitIteration(RedemptionStatus)
		If strComp(actualAmtPerUnit,strAmtPerUnit) = 0 Then
			Else
			Call Fnc_ExitIteration(AmtPerUnitStatus)
			If strComp(actualLevel,strLevel) = 0 Then
				CaptureScreenShot("")
				Else 
				Call Fnc_ExitIteration(LevelStatus)
			End If
		End If
	End If
End If
Call WriteLog("เจ้าหน้าที่ CRM สร้างข้อมูลของกำนัล","")
CaptureScreenShot("")
'=========================== End เจ้าหน้าที่ CRM สร้างข้อมูลของกำนัล ===============================

'====================== Start เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล ===========================

If Browser("Browser").Page("Page").WebEdit("productName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("productName").Set strProductName
	Browser("Browser").Page("Page").WebEdit("ccMproductName").Set strCcMproductName
	Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Click
	Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Click
	Browser("Browser").Page("Page").WebElement("กรุณาเลือก_2").Click
	Browser("Browser").Page("Page").WebElement("Normal").Click
	Browser("Browser_2").Page("Page").WebElement("frmLoyaltyManageGiftDetail").Click
	wait 1
	Browser("Browser").Page("Page").WebElement("5 - Level 5").Click
	'Browser("Browser_2").Page("Page").WebEdit("ccPartnerBarcode").Set strPartnerBarcode
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If

wait 2
If 	Browser("Browser").Page("Page").WebEdit("promoName").Exist(3) Then
	actuaPromoName = Browser("Browser").Page("Page").WebEdit("promoName").GetROProperty("value")
	If strComp(actuaPromoName,strPromoName) = 0 Then
		Else 
		Call Fnc_ExitIteration(RedemptionStatus)
	End If
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If

If Browser("Browser").Page("Page").WebElement("btn_projectOwner").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("btn_projectOwner").Click
	If Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Exist(3) Then
		Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Set strEmployName
	End If
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
	wait 3
	Browser("Browser").Page("Page").WebElement("tblCommonUser").DoubleClick
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
If Browser("Browser").Page("Page").WebElement("กรุณาเลือก_3").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("กรุณาเลือก_3").Click
	Browser("Browser").Page("Page").WebElement("บัตรกำนัล (GIFT VOUCHERS)").Click
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").Set strStartDtDisplay
	wait 1
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").Set strEndDtDisplay
	CaptureScreenShot("")
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 1
If Browser("Browser").Page("Page").WebElement("btn_workItem").Exist(3) = true Then
	Browser("Browser").Page("Page").WebElement("btn_workItem").Click
	Browser("Browser").Page("Page").WebButton("สร้าง").Click
	Browser("Browser").Page("Page").WebEdit("workItemCd_2").Set strWorkItemCd
	wait 1
	Browser("Browser").Page("Page").WebEdit("workItemName_2").Set strWorkItemName
	Browser("Browser_2").Page("Page").WebElement("btn_workgroupName_frmModal").Click
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If

wait 3
If Browser("Browser").Page("Page").WebButton("สร้าง_2").Exist(3) = true Then
	Browser("Browser").Page("Page").WebButton("สร้าง_2").Click
	wait 1
	Browser("Browser").Page("Page").WebEdit("workgroupCd_2").Set strWorkgroupCd
	wait 1
	Browser("Browser").Page("Page").WebEdit("workgroupName_2").Set strWorkgroupName
	Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
	Browser("Browser").Page("Page").WebButton("ตกลง").Click
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 1
Browser("Browser").Page("Page").WebButton("ปิด").Click
If Browser("Browser").Page("Page").WebEdit("workItemCd_2").Exist(3) = true Then
	Browser("Browser").Page("Page").WebEdit("workItemCd_2").Set strWorkItemCd
	Browser("Browser").Page("Page").WebEdit("workItemName").Set strWorkItemName
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 1
Browser("Browser").Page("Page").WebElement("btn_workgroupName_modal").Click
If Browser("Browser").Page("Page").WebEdit("workgroupCd").Exist(3) = true Then
	Browser("Browser").Page("Page").WebEdit("workgroupCd").Set strWorkgroupCd
	wait 1
	Browser("Browser").Page("Page").WebEdit("workgroupName").Set strWorkgroupName
	wait 1
	Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 1
If Browser("Browser").Page("Page").WebElement("tblCommonfindWorkGroup").Exist(3) = true Then
	Browser("Browser").Page("Page").WebElement("tblCommonfindWorkGroup").DoubleClick
	If Browser("Browser").Page("Page").WebButton("ค้นหา_2").Exist(3) = true Then
		Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
		wait 1
		Browser("Browser").Page("Page").WebButton("เลือก").Click
	End If
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 1
If Browser("Browser").Page("Page").WebCheckBox("ccAllowScFlg").Exist(5) Then
	Browser("Browser").Page("Page").WebCheckBox("ccAllowScFlg").Set "ON"
	Browser("Browser").Page("Page").WebCheckBox("ccAllowCrmFlg").Set "ON"
	Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").Set strMobileChannelFlg @@ hightlight id_;_Browser("Browser 3").Page("Page").WebElement("WebElement")_;_script infofile_;_ZIP::ssf15.xml_;_
	Browser("Browser").Page("Page").WebElement("mobiledetail_manage").Click
	Browser("Browser").Page("Page").WebElement("Display Only").Click
	Browser("Browser").Page("Page").WebEdit("ccMshortDetail").Set strMshortDetail @@ hightlight id_;_Browser("Browser 2").Page("Page").WebEdit("ccMshortDetail")_;_script infofile_;_ZIP::ssf11.xml_;_
	Browser("Browser").Page("Page").WebEdit("ccMdetail").Set strMdetai
	Browser("Browser").Page("Page").WebCheckBox("ccKioskChannelFlg").Set "ON"
	Browser("Browser").Page("Page").WebCheckBox("ccWebChannelFlg").Set "ON"
	Browser("Browser").Page("Page").WebEdit("ccWebShortDetail").Set strMshortDetail
	Browser("Browser").Page("Page").WebEdit("ccWebDetail").Set strMdetai
	Browser("Browser_3").Page("Page").WebRadioGroup("ccSendingFlg").Select "#2" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebRadioGroup("ccSendingFlg")_;_script infofile_;_ZIP::ssf17.xml_;_
	Browser("Browser_3").Page("Page").WebCheckBox("ccSelfReceiveFlg").Set strSelfReceiveFlg @@ hightlight id_;_Browser("Browser 3").Page("Page").WebCheckBox("ccSelfReceiveFlg")_;_script infofile_;_ZIP::ssf19.xml_;_
	Browser("Browser_3").Page("Page").WebCheckBox("ccPostReceiveFlg").Set strPostReceiveFlg @@ hightlight id_;_Browser("Browser 2").Page("Page").WebCheckBox("ccMobileChannelFlg")_;_script infofile_;_ZIP::ssf10.xml_;_
	Browser("Browser").Page("Page").WebEdit("description").Set "ทดสอบ"
	Browser("Browser").Page("Page").WebEdit("ccNotifyText").Set "ทดสอบ"
	Browser("Browser").Page("Page").WebEdit("WebEdit").Set "0944788232"
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 2
If Browser("Browser").Page("Page").WebButton("บันทึก_3").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("บันทึก_3").Click
	Browser("Browser_3").Page("Page").WebButton("ตกลง").Click
	Browser("Browser").Page("Page").WebButton("เพิ่มเติม/แก้ไข/แสดงรายละเอียด").Highlight
	Browser("Browser").Page("Page").WebButton("เพิ่มเติม/แก้ไข/แสดงรายละเอียด").WaitProperty "visible",true, 60000
	If Browser("Browser").Page("Page").WebButton("เพิ่มเติม/แก้ไข/แสดงรายละเอียด").Exist(5) Then
		Browser("Browser").Page("Page").WebButton("เพิ่มเติม/แก้ไข/แสดงรายละเอียด").Click
	End If
	Browser("Browser").Page("Page").WebEdit("productName_2").Set "ทดสอบ Head"
	Browser("Browser").Page("Page").WebEdit("ccKioskShortDetail").Set "ทดสอบ Sub Head"
	Browser("Browser").Page("Page").WebEdit("ccKioskDetail").Set "ของกำนัลแสดงผ่านช่องทาง Kiosk"
	Browser("Browser").Page("Page").WebButton("บันทึก").Click
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
wait 1
If Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetail_imgFile").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetail_imgFile").Click
	Browser("Browser_3").Page("Page").WebFile("imgFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("imgFile")_;_script infofile_;_ZIP::ssf29.xml_;_
	Browser("Browser_3").Page("Page").WebFile("imgThumb").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("imgThumb")_;_script infofile_;_ZIP::ssf30.xml_;_
	Browser("Browser_3").Page("Page").WebFile("infoFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg"
	wait 1
	Browser("Browser_3").Page("Page").WebFile("mmLargeFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("mmLargeFile")_;_script infofile_;_ZIP::ssf32.xml_;_
	Browser("Browser_3").Page("Page").WebFile("mmSmallFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("mmSmallFile")_;_script infofile_;_ZIP::ssf33.xml_;_
	wait 1
	Browser("Browser_3").Page("Page").WebButton("Upload").Click @@ hightlight id_;_Browser("Browser 3").Page("Page").WebElement(".avatar-circle { -moz-border-r 2")_;_script infofile_;_ZIP::ssf40.xml_;_
	wait 3
	Browser("Browser").Page("Page").WebEdit("WebEdit").Set strMobilePhone
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
CaptureScreenShot("")
Call WriteLog("เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล","")
'====================== End เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล ==============================

'============================= Start บันทึกเงื่อนไขการชำระของกำนัล =================================
If Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_prog").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_prog").Click
	If Browser("Browser_3").Page("Page").WebEdit("programName").Exist(3) Then
		Browser("Browser_3").Page("Page").WebEdit("programName").Set strProgramName
		Browser("Browser_3").Page("Page").WebButton("ค้นหา").Click
		wait 2
		Browser("Browser").Page("Page").WebButton("เลือก").Click
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกเงื่อนไขการชำระของกำนัล")
End If

If Browser("Browser_5").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_pointType").Exist(3) Then
	Browser("Browser_5").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_pointType").Click
	If Browser("Browser_3").Page("Page").WebEdit("pointTypeName").Exist(3) Then	
	Browser("Browser_3").Page("Page").WebEdit("pointTypeName").Set strPointTypeName @@ hightlight id_;_Browser("Browser 3").Page("Page").WebEdit("pointTypeName")_;_script infofile_;_ZIP::ssf45.xml_;_
	If Browser("Browser").Page("Page").WebButton("ค้นหา_2").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
	End If @@ hightlight id_;_Browser("Browser 3").Page("Page").WebButton("ค้นหา")_;_script infofile_;_ZIP::ssf46.xml_;_
	wait 2
	Browser("Browser").Page("Page").WebButton("เลือก").Click
	End If
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกเงื่อนไขการชำระของกำนัล")
End If

If Browser("Browser").Page("Page").WebElement("เงื่อนไขการชำระ").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("เงื่อนไขการชำระ").Click
	Browser("Browser").Page("Page").WebElement("คะแนนอย่างเดียว").Click
	Browser("Browser").Page("Page").WebElement("ประเภทรายการ").Click
	Browser("Browser").Page("Page").WebElement("Redemption-Product").Click
	Browser("Browser").Page("Page").WebEdit("points").Set strPoint
	Browser("Browser").Page("Page").WebEdit("partnerPoint").Set strPartnerPoint
	Browser("Browser").Page("Page").WebEdit("description_2").Set strConditionDescription
	If Browser("Browser").Page("Page").WebButton("บันทึก_4").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("บันทึก_4").Click
		wait 3
		If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) Then
			Browser("Browser").Page("Page").WebButton("ตกลง").Click
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกเงื่อนไขการชำระของกำนัล")
End If
CaptureScreenShot("")
Call WriteLog("บันทึกเงื่อนไขการชำระของกำนัล","")
'============================= End บันทึกเงื่อนไขการชำระของกำนัล ===================================

'================================= Start บันทึกของกำนัลคงคลัง ====================================

If Browser("Browser").Page("Page").Link("Product On-Hand").Exist(3) Then
	Browser("Browser").Page("Page").Link("Product On-Hand").Click
End If
wait 2
If Browser("Browser").Page("Page").WebButton("เพิ่ม").Exist(3) Then
	Browser("Browser").Page("Page").WebButton("เพิ่ม").Click
	wait 1
	If Browser("Browser").Page("Page").WebElement("กรุณาเลือก_5").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก_5").Click
		wait 1
		Browser("Browser_5").Page("Page").WebElement("นำของกำนัลเข้าคลัง").Click
		If Browser("Browser_5").Page("Page").WebEdit("qty").Exist(3) Then
			Browser("Browser_5").Page("Page").WebEdit("qty").Set strQuantity
			Browser("Browser_5").Page("Page").WebEdit("comment").Set strCommentQty
			wait 1
			If Browser("Browser").Page("Page").WebButton("บันทึก").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("บันทึก").Click
				wait 1
				If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
					Browser("Browser").Page("Page").WebButton("ตกลง").Click
				End If
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกของกำนัลคงคลัง","","","")
End If
CaptureScreenShot("")
Call WriteLog("บันทึกของกำนัลคงคลัง","")
'================================= END บันทึกของกำนัลคงคลัง ====================================

'============================== Start จัดสรรของกำนัลคงคลังให้ ศูนย์บริการลูกค้า =======================

onHandQty = Browser("Browser").Page("Page").WebElement("tbLoyaltyManageGiftDetailPohTab_onHandQty").GetROProperty("innertext")
wait 2
If Browser("Browser").Page("Page").Link("Product Allocation").Exist(5) Then
	Browser("Browser").Page("Page").Link("Product Allocation").Click
	wait 3
	If Browser("Browser").Page("Page").WebButton("เพิ่ม_2").Exist(5) = true Then
		Browser("Browser").Page("Page").WebButton("เพิ่ม_2").Click
	End If
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : จัดสรรของกำนัลคงคลังให้ ศูนย์บริการลูกค้า")
End If
CaptureScreenShot("")
Call WriteLog("จัดสรรของกำนัลคงคลังให้ ศูนย์บริการลูกค้า","")
wait 2
If Browser("Browser").Page("Page").WebElement("btn_allocation_product_serviceCenter").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("btn_allocation_product_serviceCenter").Click
	wait 1
 	If Browser("Browser_5").Page("Page").WebEdit("cscServiceCenterName").Exist(3) Then
 		Browser("Browser_5").Page("Page").WebEdit("cscServiceCenterName").Set strServiceCenterName
 		wait 1
		Browser("Browser").Page("Page").WebButton("ค้นหา_3").Click
		If 	Browser("Browser").Page("Page").WebElement("tblCommonServiceCenter").Exist(3) Then
			Browser("Browser").Page("Page").WebElement("tblCommonServiceCenter").DoubleClick
		End If
		If Browser("Browser").Page("Page").WebEdit("originalQty").Exist(3) Then
			Browser("Browser").Page("Page").WebEdit("originalQty").Set strOriginalQty
			wait 2
			Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
			If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ตกลง").Click
			End If
		End If
 	End If
'	Browser("Browser").Page("Page").WebElement("tblCommonServiceCenter").DoubleClick
'	Browser("Browser").Page("Page").WebEdit("originalQty").Set strOriginalQty
'	Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
'	Browser("Browser").Page("Page").WebButton("ตกลง").Click
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : จัดสรรของกำนัลคงคลังให้ ศูนย์บริการลูกค้า")
End If

'If Browser("Browser").Page("Page").WebButton("ค้นหา_3").Exist(3) Then
'	Browser("Browser").Page("Page").WebButton("ค้นหา_3").Click
'	Browser("Browser").Page("Page").WebElement("tblCommonServiceCenter").DoubleClick
'	Browser("Browser").Page("Page").WebEdit("originalQty").Set strOriginalQty
'	Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
'	Browser("Browser").Page("Page").WebButton("ตกลง").Click
'End If
'Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
'Browser("Browser").Page("Page").WebButton("ตกลง").Click

CaptureScreenShot("")
Call WriteLog("จัดสรรของกำนัลคงคลังให้ ศูนย์บริการลูกค้า","")
'============================== End จัดสรรของกำนัลคงคลังให้ ศูนย์บริการลูกค้า ==========================
'======================= Start บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า ============================
wait 2
If Browser("Browser").Page("Page").Link("Product Quota").Exist(3) Then
	Browser("Browser").Page("Page").Link("Product Quota").Click
	If Browser("Browser").Page("Page").WebButton("เพิ่ม_3").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("เพิ่ม_3").Click
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก_4").Click
		If Browser("Browser_5").Page("Page").WebElement("ตัวแทน/พนักงาน").Exist(3) Then
			Browser("Browser_5").Page("Page").WebElement("ตัวแทน/พนักงาน").Click
			Browser("Browser").Page("Page").WebEdit("quota").Set strQuota
			Browser("Browser").Page("Page").WebButton("บันทึก_5").Click
			If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
				Browser("Browser").Page("Page").WebButton("ตกลง").Click
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า")
End If
CaptureScreenShot("")
Call WriteLog("บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า","")
If 	Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").Highlight
	Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").Click
	If Browser("Browser").Page("Page").WebElement("เปิดใช้งาน").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("เปิดใช้งาน").Click
		Browser("Browser").Page("Page").WebButton("บันทึก_3").Click
		wait 2
		If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) = true Then
			Browser("Browser").Page("Page").WebButton("ตกลง").Click
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า")
End If
CaptureScreenShot("")
Call WriteLog("บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า","")
'========================== End บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า ===========================

'===================================== Start ค้นหาของกำนัล ======================================
wait 3
If Browser("Browser").Page("Page").WebElement("แลกของกำนัล").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("แลกของกำนัล").Click
	If Browser("Browser").Page("Page").Link("ของกำนัล").Exist(3) Then
		Browser("Browser").Page("Page").Link("ของกำนัล").Click
		wait 3
		If Browser("Browser").Page("Page").WebEdit("searchProductName").Exist(3) Then
		wait 1
		tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebEdit("searchProductName").Exist(5) = True
				Wait 1
				If tmp_timeout = 10 Then
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
			Browser("Browser").Page("Page").WebEdit("searchProductName").Set strProductName
			If Browser("Browser").Page("Page").WebButton("ค้นหา_4").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ค้นหา_4").Click
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : ค้นหาของกำนัล")
End If
CaptureScreenShot("")
Call WriteLog("ค้นหาของกำนัล","")
'===================================== End ค้นหาของกำนัล ========================================
