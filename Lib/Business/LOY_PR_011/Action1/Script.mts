﻿'============================== Start data table to variable ==============================

dataRow = Parameter("DTRow")
strCitizenID = DataTable("CitizenID",dtglobalsheet)
strProductName = DataTable("ProductName",dtglobalsheet)
strTheOneCardNo = DataTable("TheOneCardNo",dtglobalsheet)
strQuantity = DataTable("Quantity",dtglobalsheet)

'============================== End data table to variable =================================

'============================== Start Login Application ====================================

RunAction "MemberLogin", oneIteration, dataRow

'============================== End Login Application ======================================

'============================== ค้นหาลูกค้าที่ต้องการแลกของกำนัล ===================================

tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebTable("ลำดับ").RowCount > 1
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Loading Home My Task page loading for long","")
		Call Fnc_ExitIteration("Loading Home My Task page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
Call WriteLog("Loading Home My Task Completed","")	
CaptureScreenShot("")

If Browser("Browser").Page("Page").WebButton("toolbar_walkin").Exist(3) Then
	Browser("Browser").Page("Page").WebButton("toolbar_walkin").Click
End If
'wait 3
'actualAgentState = Browser("Browser").Page("Page").WebElement("toolbar_agent_state").GetROProperty("innertext")
'If strComp(actualAgentState,"Agent State : Walk-in") = 0 Then
'	Else
'	Call Fnc_ExitIteration("AgentState is" & " " &actualAgentState)
'End If
wait 2
If Browser("Browser").Page("Page").WebEdit("citizenId").Exist(3) Then
	Browser("Browser").Page("Page").WebEdit("citizenId").Set strCitizenID
	If Browser("Browser").Page("Page").WebButton("ค้นหา").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("ค้นหา").Click
		tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebTable("สิ้นสุดสถานภาพ โดย CRM").RowCount > 1
				Wait 1
				If tmp_timeout = 10 Then
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
			Browser("Browser").Page("Page").WebElement("tblloyalty_member_search_result").DoubleClick
	End If
		tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebTable("ลำดับ_2").RowCount > 1
				Wait 1
				If tmp_timeout = 10 Then
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
	If Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Exist(3) Then
		Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Click
		If Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(3) Then
			Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
			wait 3
			If Browser("Browser").Page("Page").WebButton("สร้าง").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("สร้าง").Click
			End If
			tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebTable("ลำดับ_3").RowCount > 1
				Wait 1
				If tmp_timeout = 10 Then
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
			wait 3
'			If Browser("Browser").Page("Page").WebElement("frmLoyalty_redeem_result_rdmHeader").Exist(3) = true Then
'				Browser("Browser").Page("Page").WebElement("frmLoyalty_redeem_result_rdmHeader").Click
'				wait 1
'				Browser("Browser").Page("Page").WebElement("ติดต่อด้วยตนเอง").Click	
'			End If
		End If	
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : ค้นหาลูกค้าที่ต้องการแลกของกำนัล")
End If

actual_search_result = Browser("Browser").Page("Page").WebTable("สิ้นสุดสถานภาพ โดย CRM").RowCount 
Call WriteLog("ค้นหาลูกค้าที่ต้องการแลกของกำนัล","")	
CaptureScreenShot("")

wait 1
'============================== ค้นหาของกำนัลที่ต้องการ ==========================================
If Browser("Browser").Page("Page").WebElement("redeem_result_btn_shopping").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("redeem_result_btn_shopping").Click
	tmp_timeout = 0
	Do until Browser("Browser").Page("Page").WebTable("ชื่อของกำนัล").RowCount > 1
		Wait 1
		If tmp_timeout = 10 Then
			Exit Do
		End If
		tmp_timeout = tmp_timeout + 1
	Loop
	If Browser("Browser").Page("Page").WebEdit("search_product_name").Exist(3) Then
		Browser("Browser").Page("Page").WebEdit("search_product_name").Set strProductName
		wait 1
		Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
		wait 1
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : ค้นหาของกำนัลที่ต้องการ")
End If
Call WriteLog("ค้นหาของกำนัลที่ต้องการ","")	
CaptureScreenShot("")

'============================== ทดสอบแลกของกำนัล =============================================
If Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Highlight
	Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Click
	wait 2
	If Browser("Browser").Page("Page").WebEdit("quantity").Exist(3) = true Then
		Browser("Browser").Page("Page").WebEdit("quantity").Set strQuantity
		If Browser("Browser").Page("Page").WebEdit("WebEdit").Exist(3) Then
			wait 2
			Browser("Browser").Page("Page").WebEdit("WebEdit").Set strTheOneCardNo
			If Browser("Browser").Page("Page").WebButton("ตรวจสอบ").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ตรวจสอบ").Click
			End If
			
			wait 3
			If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ตกลง").Click
				wait 3
				Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Click
			End If					
		End If
	End If
		tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebTable("ลำดับ_3").RowCount > 1
				Wait 1
				If tmp_timeout = 10 Then
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
		If Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Exist(3) Then
			Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Click
			If Browser("Browser").Page("Page").WebButton("ใช่").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ใช่").Click
				wait 3
				If Browser("Browser").Page("Page").WebButton("ตกลง_2").Exist(3) Then
					Browser("Browser").Page("Page").WebButton("ตกลง_2").Click
				End If
			End If
		End If
		Else
		CaptureScreenShot("")
		Call Fnc_ExitIteration("FAIL : ทดสอบแลกของกำนัล")
End If
Call WriteLog("ทดสอบแลกของกำนัล","")	
CaptureScreenShot("")
'============================== ตรวจสอบการบันทึกเรื่องที่ให้บริการ ===================================

'Compare
'- เลขที่รายการแลกของกำนัล (Running No. อัตโนมัติ)
'- ของกำนัลที่ต้องการแลก = ""TL 1คะแนนแลกTheOne10 คะแนน""
'- จำนวนที่ต้องการแลก = 1
'- เงื่อนไขการชำระ = คะแนนอย่างเดียว
'- คะแนนที่ใช้ = 1
'- สถานะ = ดำเนินการเรียบร้อยแล้ว
'- โปรโมชั่น = แลกของกำนัลทั่วไป
wait 3
If Browser("Browser").Page("Page").Link("เรื่องที่ให้บริการ").Exist(5) Then
	Browser("Browser").Page("Page").Link("เรื่องที่ให้บริการ").Click
	tmp_timeout = 0
			Do until Browser("Browser").Page("Page").WebTable("ลำดับ_4").RowCount > 1
				Wait 1
				If tmp_timeout = 10 Then
					Exit Do
				End If
				tmp_timeout = tmp_timeout + 1
			Loop
For Iterator = 2 To 8
	actualServiceSubject = Browser("Browser").Page("Page").WebTable("ลำดับ_4").GetCellData(2,Iterator)
	Call WriteLog("ตรวจสอบการบันทึกเรื่องที่ให้บริการ" & " "& actualServiceSubject,"")	
Next

'	actualNumOfRedeem = Browser("Browser").Page("Page").WebTable("ลำดับ_4").GetCellData(2,4)
'	actualRedeemConidition = Browser("Browser").Page("Page").WebTable("ลำดับ_4").GetCellData(2,5)
'	actualPointToRedeem = Browser("Browser").Page("Page").WebTable("ลำดับ_4").GetCellData(2,6)
'	actualRedeemStatus = Browser("Browser").Page("Page").WebTable("ลำดับ_4").GetCellData(2,7)
'	actualRedeemPromo = Browser("Browser").Page("Page").WebTable("ลำดับ_4").GetCellData(2,8)
	
End If
Call WriteLog("ตรวจสอบการบันทึกเรื่องที่ให้บริการ","")	
CaptureScreenShot("")
'============================== บันทึกผลการติดต่อ ==============================================
If Browser("Browser").Page("Page").Link("ประวัติการติดต่อ").Exist(5) Then
	Browser("Browser").Page("Page").Link("ประวัติการติดต่อ").Click
	If Browser("Browser").Page("Page").Link("สรุปการติดต่อ").Exist(3) Then
		Browser("Browser").Page("Page").Link("สรุปการติดต่อ").Click
	End If
	tmp_timeout = 0
	Do until Browser("Browser").Page("Page").WebTable("ชื่อของกำนัล").RowCount > 1
		Wait 1
		If tmp_timeout = 10 Then
			Exit Do
		End If
		tmp_timeout = tmp_timeout + 1
	Loop @@ hightlight id_;_Browser("Browser").Page("Page").WebButton("toolbar walkin")_;_script infofile_;_ZIP::ssf1.xml_;_
	
	RedemptionNo = Browser("Browser").Page("Page").WebTable("ชื่อของกำนัล").GetCellData(2,2)
	
	If Browser("Browser").Page("Page").WebButton("toolbar_walkin").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("toolbar_walkin").Click
	End If
	tmp_timeout = 0
	Do until Browser("Browser").Page("Page").WebTable("ลำดับ").RowCount > 1
		Wait 1
		If tmp_timeout = 10 Then
			Call WriteLog("Loading Home My Task page loading for long","")
			Call Fnc_ExitIteration("Loading Home My Task page loading for long")
			CaptureScreenShot("")		'Capture Screen
			Exit Do
		End If
		tmp_timeout = tmp_timeout + 1
	Loop
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกผลการติดต่อ")
End If
Call WriteLog("บันทึกผลการติดต่อ","")	
CaptureScreenShot("")


