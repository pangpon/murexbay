﻿


dataRow = Parameter("DTRow")
userName = DataTable("Username",dtglobalsheet)
password = DataTable("Password",dtglobalsheet)
citizenID = DataTable("CitizenID",dtglobalsheet)
serviceChannel = DataTable("ServiceChannel",dtglobalsheet)
receiveConfirmChannel = DataTable("ReceiveConfirmChannel",dtglobalsheet)
quantity = DataTable("Quantity",dtglobalsheet)
ProductName = DataTable("ProductName",dtglobalsheet)
Product_Type = DataTable("Product_Type",dtglobalsheet)
WaitingAmt = DataTable("WaitingAmt",dtglobalsheet)
descriptions = DataTable("descriptions",dtglobalsheet)
campaignName = DataTable("CampaignName",dtglobalsheet)
campaignCode = DataTable("CampaignCode",dtglobalsheet)
Gift_Status = DataTable("Gift_Status",dtglobalsheet)
Subcategory	= DataTable("Subcategory",dtglobalsheet)
Subject_type = DataTable("Subject_type",dtglobalsheet)

'deliveryStatus = DataTable("DeliveryStatus",dtglobalsheet)
 
'============================== End data table to variable ================================

'OpenWeb_IE "http://10.102.60.33:8080/Loyalty/login.htm"
'OpenWeb_IE "http://10.102.63.33:8080/Loyalty/login.htm"
RunAction "MemberLogin", oneIteration, dataRow

'=================================== Start Home  ===================================

'Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออก").WaitProperty "visible",True, 60000
wait 40
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออก").Click
If  Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออกย่อย").Exist(5) Then
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออกย่อย").Click
End If

Call WriteLog("Menu Home Completed","")	
CaptureScreenShot("")

'=================================== End Home  =====================================


'=================================== Start ค้นหาแคมเปญ  =============================

If  Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ค้นหาแคมเปญ").Exist(50) Then
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ค้นหาแคมเปญ").WaitProperty "visible",True, 60000
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ค้นหาแคมเปญ").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,ค้นหาแคมเปญ loading for long")

End If


Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebEdit("campaignName").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebEdit("campaignName").Set campaignName


Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebEdit("campaignCode").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebEdit("campaignCode").Set campaignCode


Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา").Click


Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("WebTable").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("WebTable").DoubleClick

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา_แคมเปญ").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา_แคมเปญ").Click

If Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("toolbar_ready_call").Exist(50) Then
	wait 2
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("toolbar_ready_call").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,ค้นหาแคมเปญ loading for long")

End If

Call WriteLog("Menu ค้นหาแคมเปญ Completed","")	
CaptureScreenShot("")

'=================================== End ค้นหาแคมเปญ  =====================================

'=================================== Start Compare text =====================================
wait 5
Status = Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebTable("ลำดับ").GetCellData(2,5)
Statustype = Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebTable("ลำดับ").GetCellData(2,6)
Statusshort = Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebTable("ลำดับ").GetCellData(2,7)
Namecampain = Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebTable("ลำดับ").GetCellData(2,9)
campain = "แคมเปญ1"
If Namecampain = campain Then
	Call WriteLog("CompareMessage","")	
	CaptureScreenShot("")
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Data does not match,""")
End If
wait 3

Call WriteLog("Menu Compare text Completed","")	
CaptureScreenShot("")

'=================================== End Compare text =====================================

'=================================== Start Call  =====================================


Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("WebTable_2").DoubleClick

wait 2


If Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("Mobile").Exist(50) Then
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("Mobile").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Call loading for long")

End If


If 	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("โทรออก").Exist(5) Then
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ต่อสาย").Click
End If

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("WebElement").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("WebElement").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("สำเร็จ – นำเสนอข้อมูลเรียบร้อย").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("สำเร็จ – นำเสนอข้อมูลเรียบร้อย").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("บันทึก").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("บันทึก").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ตกลง").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ตกลง").Click

Call WriteLog("Menu Call Completed","")	
CaptureScreenShot("")

'=================================== End Call  =========================================


'=================================== Start สร้างเรื่องที่ให้บริการ  ==============================

If Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("สร้างเรื่องที่ให้บริการ").Exist(50) Then
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("สร้างเรื่องที่ให้บริการ").WaitProperty "visible",True, 60000
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("สร้างเรื่องที่ให้บริการ").Click
else

	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,สร้างเรื่องที่ให้บริการ loading for long")
End If



Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ช่องทางให้บริการ").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ช่องทางให้บริการ").Click
wait 2
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement(ServiceChannel).WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement(ServiceChannel).Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ประเภทเรื่อง").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ประเภทเรื่อง").Click
wait 2
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement(Subject_type).WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement(Subject_type).Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ประเภทย่อย").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("ประเภทย่อย").Click
wait 2
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement(Subcategory).WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement(Subcategory).Click
wait 2
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebEdit("description").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebEdit("description").Set descriptions

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("บันทึกรายละเอียดให้บริการ").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("บันทึกรายละเอียดให้บริการ").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ใช่").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ใช่").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ตกลง").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ตกลง").Click

Call WriteLog("Menu สร้างเรื่องที่ให้บริการ Completed","")	
CaptureScreenShot("")

'=================================== End สร้างเรื่องที่ให้บริการ  ============================== @@ hightlight id_;_Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame 2").WebElement("Agent State : Not ReadyReason")_;_script infofile_;_ZIP::ssf67.xml_;_

'=================================== Start ค้นหารายการติดต่อ  ============================


Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออก").Click
If  Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออกย่อย").Exist(50) Then
	Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("รายการโทรออกย่อย").Click
else

	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,ค้นหารายการติดต่อ loading for long")
End If
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("btnสถานะย่อย").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("btnสถานะย่อย").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("สำเร็จ – นำเสนอข้อมูลเรียบร้อย").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("สำเร็จ – นำเสนอข้อมูลเรียบร้อย").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebButton("ค้นหา").Click

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 60000


wait 3
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("WebTable_2").DoubleClick

'=================================== Start ประวัติการดำเนินการ  ==============================
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("ประวัติการดำเนินการ").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("ประวัติการดำเนินการ").Click
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 60000
Call WriteLog("Menu ประวัติการดำเนินการ Completed","")	
CaptureScreenShot("")

'=================================== End ประวัติการดำเนินการ  ==============================

'=================================== Start ประวัติติดต่อ  ==============================

Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("ประวัติติดต่อ").WaitProperty "visible",True, 60000
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").Link("ประวัติติดต่อ").Click
Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 60000

Call WriteLog("Menu ประวัติติดต่อ Completed","")	
CaptureScreenShot("")
'=================================== End ประวัติติดต่อ ==============================

'=================================== End ค้นหารายการติดต่อ  ==============================
 @@ hightlight id_;_Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame 2").WebButton("ค้นหา")_;_script infofile_;_ZIP::ssf69.xml_;_
 @@ hightlight id_;_Browser("JLO Bridge Control page").Page("JLO Bridge Control page").Frame("Frame 2").WebElement("กรุณารอสักครู่...")_;_script infofile_;_ZIP::ssf70.xml_;_
