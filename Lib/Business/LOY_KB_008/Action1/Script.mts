﻿'============================== Start data table to variable ==============================

dataRow = Parameter("DTRow")
ProductName = DataTable("ProductName",dtglobalsheet)
Item_Type = DataTable("Item_Type",dtglobalsheet)
Gift_Status = DataTable("Gift_Status",dtglobalsheet)
Gift_Status2 = DataTable("Gift_Status2",dtglobalsheet)
Title = DataTable("Title",dtglobalsheet)
Detail_treasury = DataTable("CommentQty",dtglobalsheet)
ShortProductName = DataTable("DocumentName",dtglobalsheet)
Product_Category = DataTable("Product_Category",dtglobalsheet)
Points = DataTable("Point",dtglobalsheet)
startDtDisplay = DataTable("StartDtDisplay",dtglobalsheet)
endDtDisplay = DataTable("EndDtDisplay",dtglobalsheet)
HouseNo = DataTable("HouseNo",dtglobalsheet)
Road = DataTable("Road",dtglobalsheet)
ZipCode = DataTable("ZipCode",dtglobalsheet)
MobileNumber = DataTable("MobileNumber",dtglobalsheet)
Unit = DataTable("descriptions",dtglobalsheet)


 
'============================== End data table to variable ================================

RunAction "MemberLogin", oneIteration, dataRow


'============================== Start Menu องค์ความรู้  ================================

If  Browser("Browser").Page("Page").Link("จัดการองค์ความรู้").Exist(50) Then	
	Browser("Browser").Page("Page").Link("จัดการองค์ความรู้").Click
	If Browser("Browser").Page("Page").Link("สาขา").Exist(3)	Then
		Browser("Browser").Page("Page").Link("สาขา").Click
		wait 1
		If Browser("Browser").Page("Page").Link("สาขากทม").Exist(3) Then
			Browser("Browser").Page("Page").Link("สาขากทม").Click
		End If
	End If
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Menu loading for long")
End If

'Browser("Browser").Page("Page").WebElement("BTN_BK").WaitProperty "visible",true, 10000
'Browser("Browser").Page("Page").WebElement("BTN_BK").Click
'wait 1
'Browser("Browser").Page("Page").WebElement("btnbk").WaitProperty "visible",true, 10000
'Browser("Browser").Page("Page").WebElement("btnbk").Click
'wait 1


If  Browser("Browser").Page("Page").Link("กรุงเทพมหานคร").Exist(10) Then
	Browser("Browser").Page("Page").Link("กรุงเทพมหานคร").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").Link("กรุงเทพมหานคร").Click
else

	Browser("Browser").Page("Page").WebElement("BTN_BK").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("BTN_BK").Click

	Browser("Browser").Page("Page").WebElement("btnbk").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("btnbk").Click

	Browser("Browser").Page("Page").Link("กรุงเทพมหานคร").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").Link("กรุงเทพมหานคร").Click
End If







'============================== Start สร้างองค์ความรู้ใหม่ประเภทสาขา ========================

wait 5
If  Browser("Browser").Page("Page").WebButton("สร้างข้อมูลสาขา").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("สร้างข้อมูลสาขา").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebButton("สร้างข้อมูลสาขา").Click
	'Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
	If Browser("Browser").Page("Page").WebElement("ไม่พบข้อมูลที่ต้องการในระบบ").Exist(5) Then
	Call Fnc_ExitIteration("Timeout,Search Customer loading for long")
	End If
End If


If  Browser("Browser").Page("Page").WebEdit("WebEdit").Exist(5) Then
	'Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
	Browser("Browser").Page("Page").WebEdit("WebEdit").Set ProductName
End If



If  Browser("Browser").Page("Page").WebElement("ประเภทข้อมูล").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("ประเภทข้อมูล").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("ประเภทข้อมูล").Click
	wait 2
	Browser("Browser").Page("Page").WebElement(Item_Type).Click
End If


If Browser("Browser").Page("Page").WebElement("select2-drop-mask").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("select2-drop-mask").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("select2-drop-mask").Click
	wait 2
	Browser("Browser").Page("Page").WebElement(Gift_Status).Click
End If


Browser("Browser").Page("Page").WebEdit("Datestart").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("Datestart").Set startDtDisplay

Browser("Browser").Page("Page").WebEdit("Dateend").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("Dateend").Set endDtDisplay

Browser("Browser").Page("Page").WebEdit("__:__:__").WaitProperty "visible",true, 10000 @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("  :  :  ")_;_script infofile_;_ZIP::ssf27.xml_;_
Browser("Browser").Page("Page").WebEdit("__:__:__").Set "00:00:00" @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("  :  :  ")_;_script infofile_;_ZIP::ssf28.xml_;_

Browser("Browser").Page("Page").WebEdit("__:__:___2").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("__:__:___2").Set "00:00:00"


 @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("ตกลง")_;_script infofile_;_ZIP::ssf38.xml_;_
Browser("Browser").Page("Page").WebEdit("ที่อยู่").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("ที่อยู่").Set HouseNo
Browser("Browser").Page("Page").WebEdit("ถนน").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("ถนน").Set Road
Browser("Browser").Page("Page").WebButton("WebButton_zipcode").Click
wait 2
Browser("Browser").Page("Page").WebEdit("zipcode").Set ZipCode
Browser("Browser").Page("Page").WebButton("ค้นหาที่อยู่").Click
wait 2
Browser("Browser").Page("Page").WebElement("select_zipcode").DoubleClick
Browser("Browser").Page("Page").WebEdit("phone").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("phone").Set MobileNumber


Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click

If  Browser("Browser").Page("Page").WebElement("ดำเนินการเรียบร้อย").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("ดำเนินการเรียบร้อย").Click
End If

Browser("Browser").Page("Page").WebButton("ตกลง").Click

'============================== End สร้างองค์ความรู้ใหม่ประเภทสาขา ========================



'============================== End Menu องค์ความรู้  ================================

'============================== Start ค้นหาองค์ความ =================================
If  Browser("Browser").Page("Page").Link("จัดการองค์ความรู้").Exist(50) Then	
	Browser("Browser").Page("Page").Link("จัดการองค์ความรู้").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Menu loading for long")
End If
Browser("Browser").Page("Page").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").Link("ค้นหา_องค์ความรู้").Click
'Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
Browser("Browser").Page("Page").WebEdit("title").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("title").Set ProductName
Browser("Browser").Page("Page").WebElement("ค้นหาข้อมูลสาขา").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebElement("ค้นหาข้อมูลสาขา").Click


'============================== End ค้นหาองค์ความ =================================


'============================== Start สรุป ======================================


Browser("Browser").Page("Page").Link("สรุป").Click
Browser("Browser").Page("Page").WebEdit("สรุป").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("สรุป").Set Unit
Browser("Browser").Page("Page").WebButton("บันทึกสรุป").Click
'Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click


'============================== End สรุป ======================================

'=================================== Start เอกสารแนบ ===============================

If  Browser("Browser").Page("Page").Link("เอกสารแนบ").Exist(50) Then
	Browser("Browser").Page("Page").Link("เอกสารแนบ").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,แนบเอกสาร loading for long")
End If
wait 3
On error resume next  
DocumentCount = Browser("Browser").Page("Page").WebTable("ลำดับ_2").RowCount
actualRowData = Browser("Browser").Page("Page").WebElement("kbMgmtBranch_cbMainFlg").GetROProperty("innertext")
If  actualRowData <> 1 Then
	Else
	
	If  DocumentCount >= 2 Then
		For Iterator = 2 To DocumentCount
			If Browser("Browser").Page("Page").WebElement("kbMgmtBranch_cbMainFlg").Exist(3) Then
				Browser("Browser").Page("Page").WebElement("kbMgmtBranch_cbMainFlg").Click
			End If
			If Browser("Browser").Page("Page").WebButton("ลบ").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ลบ").Click
			End If
			If Browser("Browser").Page("Page").WebButton("ยืนยัน").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("ยืนยัน").Click
			End If
		Next
	End If
End If

Browser("Browser").Page("Page").WebButton("สร้างแนบเอกสาร").WaitProperty "visible",true, 10000
wait 5
Browser("Browser").Page("Page").WebButton("สร้างแนบเอกสาร").Click

Browser("Browser").Page("Page").WebElement("Selectimage").WaitProperty "visible",true, 10000
wait 5
Browser("Browser").Page("Page").WebElement("Selectimage").Click @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("WebButton")_;_script infofile_;_ZIP::ssf23.xml_;_
wait 3
Browser("Browser").Page("Page").WebFile("file").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser").Page("Page 2").WebFile("file")_;_script infofile_;_ZIP::ssf24.xml_;_

If  Browser("Browser").Page("Page").WebButton("อัพโหลด").Exist(5) Then
	wait 2
	Browser("Browser").Page("Page").WebButton("อัพโหลด").Click
End IF

If  Browser("Browser").Page("Page").WebButton("ตกลงอัพไฟล์").Exist(5) Then
	wait 5
	Browser("Browser").Page("Page").WebButton("ตกลงอัพไฟล์").Click
End IF @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("อัพโหลด")_;_script infofile_;_ZIP::ssf25.xml_;_
 @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("ตกลง")_;_script infofile_;_ZIP::ssf26.xml_;_
wait 5
'If Browser("Browser").Page("Page").WebButton("Close").Exist(5) Then
'	Browser("Browser").Page("Page").WebButton("Close").Click
'End If



Browser("Browser").Page("Page").WebCheckBox("mainFlg").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebCheckBox("mainFlg").Set "ON"

Browser("Browser").Page("Page").WebCheckBox("sendDocTypeFlg").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebCheckBox("sendDocTypeFlg").Set "ON"

Browser("Browser").Page("Page").WebEdit("attName").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("attName").Set ShortProductName

Browser("Browser").Page("Page").WebEdit("descp").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("descp").Set Product_Category


If  Browser("Browser").Page("Page").WebButton("บันทึกแนบไฟล์").Exist(5) Then
	wait 2
	Browser("Browser").Page("Page").WebButton("บันทึกแนบไฟล์").Click
	'Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
End If

wait 1
'Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

If  Browser("Browser").Page("Page").Link("เอกสารหลัก").Exist(10) Then
	Browser("Browser").Page("Page").Link("เอกสารหลัก").Click
End If

Call WriteLog("Menu เอกสารแนบ Completed","")	
CaptureScreenShot("")
'=================================== End เอกสารแนบ ===============================

'=================================== Start คำสำคัญ ===============================

If  Browser("Browser").Page("Page").Link("คำสำคัญ").Exist(10) Then
	Browser("Browser").Page("Page").Link("คำสำคัญ").Click
End If

If Browser("Browser").Page("Page").WebButton("สร้าง_คำสำคัญ").Exist(50) Then
	Browser("Browser").Page("Page").WebButton("สร้าง_คำสำคัญ").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebButton("สร้าง_คำสำคัญ").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,แนบเอกสาร loading for long")
End If



Browser("Browser").Page("Page").WebEdit("keyword").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("keyword").Set Points
Browser("Browser").Page("Page").WebButton("บันทึก_คำสำคัญ").Click


If  Browser("Browser").Page("Page").WebButton("ตกลง").Exist(10) Then
	Browser("Browser").Page("Page").WebButton("ตกลง").Click
End If

Call WriteLog("Menu คำสำคัญ Completed","")	
CaptureScreenShot("")
'=================================== End คำสำคัญ ===============================

'=================================== Startเปิดใช้งาน =============================


If  Browser("Browser").Page("Page").WebElement("การใช้งานสาขาสถานะ").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("การใช้งานสาขาสถานะ").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("การใช้งานสาขาสถานะ").Click
	wait 2
	Browser("Browser").Page("Page").WebElement(Gift_Status2).Click
End If

Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

Call WriteLog("Menu เปิดใช้งาน Completed","")	
CaptureScreenShot("")
'=================================== End เปิดใช้งาน ===============================

'=================================== Startค้นหาองค์ความรู้ ==========================


Browser("Browser").Page("Page").Link("ค้นหา_องค์ความรู้").Click
Browser("Browser").Page("Page").WebEdit("title_องค์ความรู้").WaitProperty "visible",true, 10000
wait 2
Browser("Browser").Page("Page").WebEdit("title_องค์ความรู้").Set ProductName
Browser("Browser").Page("Page").WebButton("ค้นหา_องค์ความรู้2").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ค้นหา_องค์ความรู้2").Click

tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebTable("ลำดับ").RowCount > 2
	Wait 1
	If tmp_timeout = 10 Then
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop

Call WriteLog("Menu ค้นหาองค์ความรู้ Completed","")	
CaptureScreenShot("")

'=================================== End ค้นหาองค์ความรู้ ==========================
