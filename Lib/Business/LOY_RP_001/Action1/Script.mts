﻿'============================== Start data table to variable ==============================
strRedemptionProduct = "Redemption Product"
strRedemptionStatus = "อยู่ระหว่างดำเนินการ"
strAmtPerUnit = "1"
strLevel = "3 - Level 3"
strPromoName = "แลกของกำนัลทั่วไป"

strProductName = DataTable("ProductName",dtglobalsheet)
strCcMproductName = DataTable("CcMproductName",dtglobalsheet)
strEmployName = DataTable("EmployName",dtglobalsheet)
strStartDtDisplay = DataTable("StartDtDisplay",dtglobalsheet)
strEndDtDisplay = DataTable("EndDtDisplay",dtglobalsheet)
strWorkItemCd = DataTable("WorkItemCd",dtglobalsheet)	
strWorkItemName = DataTable("WorkItemName",dtglobalsheet)
strWorkgroupCd = DataTable("WorkgroupCd",dtglobalsheet)
strWorkgroupName = DataTable("WorkgroupName",dtglobalsheet)
strMobileChannelFlg = DataTable("MobileChannelFlg",dtglobalsheet)
strSelfReceiveFlg = DataTable("SelfReceiveFlg",dtglobalsheet)
strPostReceiveFlg = DataTable("PostReceiveFlg",dtglobalsheet)
strMshortDetail = DataTable("MshortDetail",dtglobalsheet)
strMdetai = DataTable("Mdetail",dtglobalsheet)
strProgramName = DataTable("ProgramName",dtglobalsheet)
strPointTypeName = DataTable("PointTypeName",dtglobalsheet)
strPoint = DataTable("Point",dtglobalsheet)
strQuantity = DataTable("Quantity",dtglobalsheet)
strOriginalQty	= DataTable("OriginalQty",dtglobalsheet)
strCommentQty = DataTable("CommentQty",dtglobalsheet)
strServiceCenterName = DataTable("ServiceCenterName",dtglobalsheet)
strQuota = DataTable("Quota",dtglobalsheet)

dataRow = Parameter("DTRow")
'============================== End data table to variable ================================


'============================== Start Login Application ===================================

RunAction "MemberLogin", oneIteration, dataRow

'============================== End Login Application ===================================== @@ hightlight id_;_Browser("Browser").Page("Page").Link("การแลกของกำนัล")_;_script infofile_;_ZIP::ssf3.xml_;_
 
 
'=========================== Start เจ้าหน้าที่ CRM สร้างข้อมูลของกำนัล ============================== @@ hightlight id_;_Browser("Browser").Page("Page").WebElement("WebElement")_;_script infofile_;_ZIP::ssf7.xml_;_

If Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(3) Then
	Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
	If Browser("Browser").Page("Page").Link("จัดการของกำนัล").Exist(3) Then
		Browser("Browser").Page("Page").Link("จัดการของกำนัล").Click
		Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Click
	End If
End If
Call WriteLog("เจ้าหน้าที่ CRM สร้างข้อมูลของกำนัล","")	
CaptureScreenShot("")
'=========================== End เจ้าหน้าที่ CRM สร้างข้อมูลของกำนัล =============================== @@ hightlight id_;_Browser("Browser").Page("Page").WebButton("สร้างของกำนัล")_;_script infofile_;_ZIP::ssf6.xml_;_

'======================== Start Verify Expected Page สร้างของกำนัล ==========================
actualRedemptionProduct = Browser("Browser").Page("Page").WebElement("Redemption Product").GetROProperty("innertext")
actualRedemptionStatus = Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").GetROProperty("innertext")
actualAmtPerUnit = Browser("Browser").Page("Page").WebEdit("ccAmtPerUnit").GetROProperty("value")
actualLevel = Browser("Browser").Page("Page").WebElement("3 - Level 3").GetROProperty("innertext")

If strComp(actualRedemptionProduct,strRedemptionProduct) = 0 Then
	Else 
	Call Fnc_ExitIteration("RedemptionProductStatus","","","")
	If strComp(actualRedemptionStatus,strRedemptionStatus) = 0 Then
		Else
		Call Fnc_ExitIteration("RedemptionStatus","","","")
		If strComp(actualAmtPerUnit,strAmtPerUnit) = 0 Then
			Else
			Call Fnc_ExitIteration("AmtPerUnitStatus","","","")
			If strComp(actualLevel,strLevel) = 0 Then
				CaptureScreenShot("")
				Else 
				Call Fnc_ExitIteration("LevelStatus","","","")
			End If
		End If
	End If
End If

'============================== End Verify Expected Page สร้างของกำนัล =======================

'====================== Start เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล ===========================

If Browser("Browser").Page("Page").WebEdit("productName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("productName").Set strProductName
	If Browser("Browser").Page("Page").WebEdit("ccMproductName").Exist(3) Then
		Browser("Browser").Page("Page").WebEdit("ccMproductName").Set strCcMproductName
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Click
		If Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Exist(3) Then
			Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Click
		End If
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก_2").Click
		If Browser("Browser").Page("Page").WebElement("Normal").Exist(3) Then
			Browser("Browser").Page("Page").WebElement("Normal").Click
		End If
	End If
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If

wait 2
If 	Browser("Browser").Page("Page").WebEdit("promoName").Exist(3) Then
	actuaPromoName = Browser("Browser").Page("Page").WebEdit("promoName").GetROProperty("value")
	If strComp(actuaPromoName,strPromoName) = 0 Then
		Else 
		Call Fnc_ExitIteration("RedemptionStatus","","","")
	End If
End If

If Browser("Browser").Page("Page").WebElement("btn_projectOwner").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("btn_projectOwner").Click
	If Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Exist(3) Then
		Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Set strEmployName
	End If
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
	wait 3
	Browser("Browser").Page("Page").WebElement("tblCommonUser").DoubleClick
	Else 
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
If Browser("Browser").Page("Page").WebElement("กรุณาเลือก_3").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("กรุณาเลือก_3").Click
	Browser("Browser").Page("Page").WebElement("บัตรกำนัล (GIFT VOUCHERS)").Click
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").Set strStartDtDisplay
	wait 1
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").Set strEndDtDisplay
End If

If Browser("Browser").Page("Page").WebElement("btn_workItem").Exist(3) = true Then
	Browser("Browser").Page("Page").WebElement("btn_workItem").Click
	Browser("Browser").Page("Page").WebButton("สร้าง").Click
	Browser("Browser").Page("Page").WebEdit("workItemCd_2").Set strWorkItemCd
	wait 1
	Browser("Browser").Page("Page").WebEdit("workItemName_2").Set strWorkItemName
	Browser("Browser_2").Page("Page").WebElement("btn_workgroupName_frmModal").Click
End If

wait 3
If Browser("Browser").Page("Page").WebButton("สร้าง_2").Exist(3) = true Then
	Browser("Browser").Page("Page").WebButton("สร้าง_2").Click
	Browser("Browser").Page("Page").WebEdit("workgroupCd_2").Set strWorkgroupCd
	wait 1
	Browser("Browser").Page("Page").WebEdit("workgroupName_2").Set strWorkgroupName
	Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
	Browser("Browser").Page("Page").WebButton("ตกลง").Click
End If

Browser("Browser").Page("Page").WebButton("ปิด").Click
If Browser("Browser").Page("Page").WebEdit("workItemCd_2").Exist(3) = true Then
	Browser("Browser").Page("Page").WebEdit("workItemCd_2").Set strWorkItemCd
	Browser("Browser").Page("Page").WebEdit("workItemName").Set strWorkItemName
End If

Browser("Browser").Page("Page").WebElement("btn_workgroupName_modal").Click
If Browser("Browser").Page("Page").WebEdit("workgroupCd").Exist(3) = true Then
	Browser("Browser").Page("Page").WebEdit("workgroupCd").Set strWorkgroupCd
	wait 1
	Browser("Browser").Page("Page").WebEdit("workgroupName").Set strWorkgroupName
	Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
End If

If Browser("Browser").Page("Page").WebElement("tblCommonfindWorkGroup").Exist(3) = true Then
	Browser("Browser").Page("Page").WebElement("tblCommonfindWorkGroup").DoubleClick
	If Browser("Browser").Page("Page").WebButton("ค้นหา_2").Exist(3) = true Then
		Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
		wait 1
		Browser("Browser").Page("Page").WebButton("เลือก").Click
	End If
End If

If Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").Exist(5) Then
	Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").Set strMobileChannelFlg
	If Browser("Browser").Page("Page").WebElement("mobiledetail_manage").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("mobiledetail_manage").Click
		If Browser("Browser").Page("Page").WebElement("Display & Redeem").Exist(3) Then
			Browser("Browser").Page("Page").WebElement("Display & Redeem").Click
		End If
	End If
	If Browser("Browser_3").Page("Page").WebRadioGroup("ccSendingFlg").Exist(3) Then
		Browser("Browser_3").Page("Page").WebRadioGroup("ccSendingFlg").Select "#2"
		If Browser("Browser_3").Page("Page").WebCheckBox("ccSelfReceiveFlg").Exist(3) Then
			Browser("Browser_3").Page("Page").WebCheckBox("ccSelfReceiveFlg").Set strSelfReceiveFlg
			If Browser("Browser_3").Page("Page").WebCheckBox("ccPostReceiveFlg").Exist(3) Then
				Browser("Browser_3").Page("Page").WebCheckBox("ccPostReceiveFlg").Set strPostReceiveFlg
				If Browser("Browser").Page("Page").WebEdit("ccMshortDetail").Exist(3) Then
					Browser("Browser").Page("Page").WebEdit("ccMshortDetail").Set strMshortDetail
					If Browser("Browser").Page("Page").WebEdit("ccMdetail").Exist(3) Then
						Browser("Browser").Page("Page").WebEdit("ccMdetail").Set strMdetai
						Browser("Browser").Page("Page").WebButton("บันทึก_3").Click
						wait 3
						Browser("Browser_3").Page("Page").WebButton("ตกลง").Click
					End If
				End If
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล")
End If
 @@ hightlight id_;_Browser("Browser 3").Page("Page").WebElement("WebElement")_;_script infofile_;_ZIP::ssf16.xml_;_
 If Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetail_imgFile").Exist(5) Then
 	Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetail_imgFile").Click
 	wait 1
 	Browser("Browser_3").Page("Page").WebFile("imgFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg"
	Browser("Browser_3").Page("Page").WebFile("imgThumb").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("imgThumb")_;_script infofile_;_ZIP::ssf30.xml_;_
	Browser("Browser_3").Page("Page").WebFile("infoFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg"
	wait 1
	Browser("Browser_3").Page("Page").WebFile("mmLargeFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("mmLargeFile")_;_script infofile_;_ZIP::ssf32.xml_;_
	Browser("Browser_3").Page("Page").WebFile("mmSmallFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser 3").Page("Page").WebFile("mmSmallFile")_;_script infofile_;_ZIP::ssf33.xml_;_
	wait 1
	CaptureScreenShot("")
	
	Browser("Browser").Page("Page").WebButton("Upload").Click
	wait CaptureScreenShot("")
 End If

If Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_prog").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_prog").Click
	If Browser("Browser_3").Page("Page").WebEdit("programName").Exist(3) Then
		Browser("Browser_3").Page("Page").WebEdit("programName").Set strProgramName
		Browser("Browser_3").Page("Page").WebButton("ค้นหา").Click
		wait 2
		Browser("Browser").Page("Page").WebButton("เลือก").Click
	End If
End If

If Browser("Browser_5").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_pointType").Exist(3) Then
	Browser("Browser_5").Page("Page").WebElement("btn_LoyaltyManageGiftDetailTermOfPayment_pointType").Click
	If Browser("Browser_3").Page("Page").WebEdit("pointTypeName").Exist(3) Then	
	Browser("Browser_3").Page("Page").WebEdit("pointTypeName").Set strPointTypeName @@ hightlight id_;_Browser("Browser 3").Page("Page").WebEdit("pointTypeName")_;_script infofile_;_ZIP::ssf45.xml_;_
	Browser("Browser_3").Page("Page").WebButton("ค้นหา").Click @@ hightlight id_;_Browser("Browser 3").Page("Page").WebButton("ค้นหา")_;_script infofile_;_ZIP::ssf46.xml_;_
	wait 2
	Browser("Browser").Page("Page").WebButton("เลือก").Click
	End If
End If

If Browser("Browser").Page("Page").WebElement("เงื่อนไขการชำระ").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("เงื่อนไขการชำระ").Click
	Browser("Browser").Page("Page").WebElement("คะแนนอย่างเดียว").Click
End If

If Browser("Browser").Page("Page").WebElement("ประเภทรายการ").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("ประเภทรายการ").Click
End If
If Browser("Browser").Page("Page").WebElement("Redemption-Product").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("Redemption-Product").Click
	Browser("Browser").Page("Page").WebEdit("points").Set strPoint
	If Browser("Browser").Page("Page").WebButton("บันทึก_4").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("บันทึก_4").Click
		If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) Then
			Browser("Browser").Page("Page").WebButton("ตกลง").Click
		End If
	End If
End If
If Browser("Browser").Page("Page").Link("Product On-Hand").Exist(3) Then
	Browser("Browser").Page("Page").Link("Product On-Hand").Click
End If
Call WriteLog("เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล","")	
CaptureScreenShot("")
'====================== End เจ้าหน้าที่ CRM  ระบุข้อมูลรายละเอียดของกำนัล ===========================

'================================= Start บันทึกของกำนัลคงคลัง ==================================
wait 3
If Browser("Browser").Page("Page").WebButton("เพิ่ม").Exist(3) Then
	Browser("Browser").Page("Page").WebButton("เพิ่ม").Click
	wait 1
	If Browser("Browser").Page("Page").WebElement("ประเภทรายการ_2").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("ประเภทรายการ_2").Click
		Browser("Browser_5").Page("Page").WebElement("นำของกำนัลเข้าคลัง").Click
		If Browser("Browser_5").Page("Page").WebEdit("qty").Exist(3) Then
			Browser("Browser_5").Page("Page").WebEdit("qty").Set strQuantity
			Browser("Browser_5").Page("Page").WebEdit("comment").Set strCommentQty
			If Browser("Browser").Page("Page").WebButton("บันทึก").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("บันทึก").Click
				If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
					Browser("Browser").Page("Page").WebButton("ตกลง").Click
				End If
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกของกำนัลคงคลัง")
End If
CaptureScreenShot("")

onHandQty = Browser("Browser").Page("Page").WebElement("tbLoyaltyManageGiftDetailPohTab_onHandQty").GetROProperty("innertext")
wait 2
If Browser("Browser").Page("Page").Link("Product Allocation").Exist(3) Then
	Browser("Browser").Page("Page").Link("Product Allocation").Click
	If Browser("Browser").Page("Page").WebButton("เพิ่ม_2").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("เพิ่ม_2").Click
	End If
End If
If Browser("Browser").Page("Page").WebElement("btn_allocation_product_serviceCenter").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("btn_allocation_product_serviceCenter").Click
	If Browser("Browser_5").Page("Page").WebEdit("cscServiceCenterName").Exist(3) Then
		Browser("Browser_5").Page("Page").WebEdit("cscServiceCenterName").Set strServiceCenterName
		If Browser("Browser").Page("Page").WebButton("ค้นหา_3").Exist(3) Then
			Browser("Browser").Page("Page").WebButton("ค้นหา_3").Click
			If 	Browser("Browser").Page("Page").WebElement("tblCommonServiceCenter").Exist(3) Then
					Browser("Browser").Page("Page").WebElement("tblCommonServiceCenter").DoubleClick
					Browser("Browser").Page("Page").WebEdit("originalQty").Set strOriginalQty
					Browser("Browser").Page("Page").WebButton("บันทึก_2").Click
					wait 2
					Browser("Browser").Page("Page").WebButton("ตกลง").Click
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกของกำนัลคงคลัง")
End If
Call WriteLog("บันทึกของกำนัลคงคลัง","")	
CaptureScreenShot("")

'================================= END บันทึกของกำนัลคงคลัง ============================================

'========================== Start บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า ===============================
If Browser("Browser").Page("Page").Link("Product Quota").Exist(3) Then
	Browser("Browser").Page("Page").Link("Product Quota").Click
	If Browser("Browser").Page("Page").WebButton("เพิ่ม_3").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("เพิ่ม_3").Click
		Browser("Browser").Page("Page").WebElement("ประเภทลูกค้า").Click
		If Browser("Browser_5").Page("Page").WebElement("ตัวแทน/พนักงาน").Exist(3) Then
			Browser("Browser_5").Page("Page").WebElement("ตัวแทน/พนักงาน").Click
			Browser("Browser").Page("Page").WebEdit("quota").Set strQuota
			Browser("Browser").Page("Page").WebButton("บันทึก_5").Click
			If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
				Browser("Browser").Page("Page").WebButton("ตกลง").Click
				If 	Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").Exist(3) Then
				Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").Highlight
				Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").Click
				If Browser("Browser").Page("Page").WebElement("เปิดใช้งาน").Exist(3) Then
					Browser("Browser").Page("Page").WebElement("เปิดใช้งาน").Click
					Browser("Browser").Page("Page").WebButton("บันทึก_3").Click
					wait 2
					If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) = true Then
						Browser("Browser").Page("Page").WebButton("ตกลง").Click
					End If
				End If
				End If
			End If
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า")
End If
Call WriteLog("บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า","")	
CaptureScreenShot("")
'========================== End บันทึกการจำกัดจำนวนของกำนัลตามประเภทลูกค้า ================================

'===================================== Start ค้นหาของกำนัล ===========================================
If Browser("Browser").Page("Page").WebElement("แลกของกำนัล").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("แลกของกำนัล").Click
	If Browser("Browser").Page("Page").Link("ของกำนัล").Exist(3) Then
		Browser("Browser").Page("Page").Link("ของกำนัล").Click
		If Browser("Browser").Page("Page").WebEdit("searchProductName").Exist(3) Then
			Browser("Browser").Page("Page").WebEdit("searchProductName").Set strProductName
			Browser("Browser").Page("Page").WebButton("ค้นหา_4").Click
		End If
	End If
	Else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("FAIL : ค้นหาของกำนัล")
End If
Call WriteLog("ค้นหาลูกค้าที่ต้องการแลกของกำนัล","")	
CaptureScreenShot("")
'===================================== End ค้นหาของกำนัล ===========================================

