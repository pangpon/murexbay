﻿
dataRow = Parameter("DTRow")
ProductName = datatable.Value("ProductName",dtglobalsheet)
ShortProductName = datatable.Value("CcMproductName",dtglobalsheet)
Product_Type = datatable.Value("Product_Type",dtglobalsheet)
Product_Category = datatable.Value("Product_Category",dtglobalsheet)
searchCommonUser_EmployName = datatable.Value("EmployName",dtglobalsheet)
startDtDisplay = datatable.Value("StartDtDisplay",dtglobalsheet)
endDtDisplay = datatable.Value("EndDtDisplay",dtglobalsheet)
workgroupCd = datatable.Value("WorkgroupCd",dtglobalsheet)
workgroupName = datatable.Value("WorkgroupName",dtglobalsheet)
workItemCd = datatable.Value("WorkItemCd",dtglobalsheet)
workItemName = datatable.Value("WorkItemName",dtglobalsheet)
ccMshortDetail = datatable.Value("MshortDetail",dtglobalsheet)
ccMdetail = datatable.Value("Mdetail",dtglobalsheet)
ccMexpire = datatable.Value("ccMexpire",dtglobalsheet)
Usageperiod = datatable.Value("Usageperiod",dtglobalsheet)
Points = datatable.Value("Point",dtglobalsheet)
Item_Type = datatable.Value("Item_Type",dtglobalsheet)
Unit = datatable.Value("Quantity",dtglobalsheet)
Detail_treasury = datatable.Value("CommentQty",dtglobalsheet)
Gift_Status = datatable.Value("Gift_Status",dtglobalsheet)



RunAction "MemberLogin", oneIteration, dataRow

'==============================Start Select Redeem gift=======================================

If  Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(50) Then
	Browser("Browser").Page("Page").Link("แลกของกำนัล").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Redeem gift page loading for long")
End If
CaptureScreenShot("")
Call WriteLog("Select,Redeem gift page loading completed","")
'==============================End Select Redeem gift=========================================

'==============================Start Select Manage gift=======================================

If  Browser("Browser").Page("Page").Link("จัดการของกำนัล").Exist(50) Then
	Browser("Browser").Page("Page").Link("จัดการของกำนัล").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").Link("จัดการของกำนัล").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Select Manage gift page loading for long")
End If
CaptureScreenShot("")
Call WriteLog("Select,Manage gift page loading completed","") @@ hightlight id_;_Browser("Browser").Page("Page").Link("จัดการของกำนัล")_;_script infofile_;_ZIP::ssf5.xml_;_
'==============================End Select Manage gift=========================================


'==============================Start Create gift==============================================
 
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000

If  Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Click
else
	Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("สร้างของกำนัล").Click
End If
CaptureScreenShot("")
Call WriteLog("Create,gift page loading completed","")
'==============================End Create gift================================================

'==============================Start check Description========================================



CategoryCompare = "Redemption Product"
RatingCompare = "3 - Level 3"
StatusCompare = "อยู่ระหว่างดำเนินการ"
VillageCompare = "1"
If  Browser("Browser").Page("Page").WebElement("รายละเอียดของกำนัล").Exist(50) Then
	Category = Browser("Browser").Page("Page").WebElement("Redemption Product").GetROProperty("innertext")
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,check Description page loading for long")
End If


Rating = Browser("Browser").Page("Page").WebElement("3 - Level 3").GetROProperty("innertext")

Status = Browser("Browser").Page("Page").WebElement("อยู่ระหว่างดำเนินการ").GetROProperty("innertext")

Village = Browser("Browser").Page("Page").WebEdit("ccAmtPerUnit").GetROProperty("Value")

If CategoryCompare = Category Then
	
	If	Rating = RatingCompare  Then
		
	else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Data does not match")
	End If
	
		If Status = StatusCompare Then
		
		else
		CaptureScreenShot("")
		Call Fnc_ExitIteration("Timeout,Data does not match")
		End If
		
			If Village = VillageCompare Then
				CaptureScreenShot("")
			else
			CaptureScreenShot("")
			Call Fnc_ExitIteration("Timeout,Data does not match")
			End If
else
CaptureScreenShot("")
Call Fnc_ExitIteration("Timeout,Data does not match")
End If
CaptureScreenShot("")
Call WriteLog("check,Description page loading completed","")
'============================== End check Description========================================


'============================== Start edit Gift =============================================

If  Browser("Browser").Page("Page").WebEdit("productName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("productName").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("productName").Set ProductName
else
	Browser("Browser").Page("Page").WebEdit("productName").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("productName").Set ProductName
End If

If  Browser("Browser").Page("Page").WebEdit("ccMproductName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("ccMproductName").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("ccMproductName").Set ShortProductName
else
	Browser("Browser").Page("Page").WebEdit("ccMproductName").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("ccMproductName").Set ShortProductName
End If
 @@ hightlight id_;_Browser("Browser").Page("Page").WebEdit("ccMproductName")_;_script infofile_;_ZIP::ssf8.xml_;_

Browser("Browser").Page("Page").WebElement("Gift_Type").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("Gift_Type").Click @@ hightlight id_;_Browser("Browser").Page("Page").WebElement(".avatar-circle { -moz-border-r 2")_;_script infofile_;_ZIP::ssf10.xml_;_
Browser("Browser").Page("Page").WebElement(Product_Type).WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement(Product_Type).Click


Browser("Browser").Page("Page").WebElement("Privilege_Type").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("Privilege_Type").Click
Browser("Browser").Page("Page").WebElement(Product_Category).WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement(Product_Category).Click
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
wait 2
PromotionCompare = "แลกของกำนัลทั่วไป"
wait 2
Promotion = Browser("Browser").Page("Page").WebEdit("promoName").GetROProperty("Value")

If PromotionCompare = Promotion Then

else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Data does not match")
End If

wait 2
If  Browser("Browser").Page("Page").WebButton("WebButton").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("WebButton").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("WebButton").Click
else
	Browser("Browser").Page("Page").WebButton("WebButton").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("WebButton").Click
End If
 @@ hightlight id_;_Browser("Browser").Page("Page").WebButton("WebButton")_;_script infofile_;_ZIP::ssf11.xml_;_
If  Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Set searchCommonUser_EmployName
else
	Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("searchCommonUser_EmployName").Set searchCommonUser_EmployName
End If
 @@ hightlight id_;_Browser("Browser").Page("Page").WebEdit("searchCommonUser EmployName")_;_script infofile_;_ZIP::ssf12.xml_;_
If  Browser("Browser").Page("Page").WebButton("ค้นหา").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("ค้นหา").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
else
	Browser("Browser").Page("Page").WebButton("ค้นหา").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
End If
 @@ hightlight id_;_Browser("Browser").Page("Page").WebButton("ค้นหา")_;_script infofile_;_ZIP::ssf13.xml_;_
If  Browser("Browser").Page("Page").WebElement("WebTable").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("WebTable").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebElement("WebTable").DoubleClick
else
	Browser("Browser").Page("Page").WebElement("WebTable").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebElement("WebTable").DoubleClick
End If 


If  Browser("Browser").Page("Page").WebElement("WebElement_Privilege").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("WebElement_Privilege").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebElement("WebElement_Privilege").Click
	Browser("Browser").Page("Page").WebElement("บัตรกำนัล (GIFT VOUCHERS)").Click
else
	Browser("Browser").Page("Page").WebElement("WebElement_Privilege").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebElement("WebElement_Privilege").Click
	Browser("Browser").Page("Page").WebElement("บัตรกำนัล (GIFT VOUCHERS)").Click
End If



If  Browser("Browser").Page("Page").WebEdit("startDtDisplay").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").Click
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").Set startDtDisplay
else
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").Click
	Browser("Browser").Page("Page").WebEdit("startDtDisplay").Set startDtDisplay
End If

If  Browser("Browser").Page("Page").WebEdit("endDtDisplay").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").Click
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").Set endDtDisplay
else
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").Click
	Browser("Browser").Page("Page").WebEdit("endDtDisplay").Set endDtDisplay
End If
CaptureScreenShot("")
Call WriteLog("edit,Gift page loading completed","")

'============================== End edit Gift =============================================

'===================================Start สร้าง ชิ้นงาน =======================================

wait 2
If  Browser("Browser").Page("Page").WebButton("Work").Exist(50) Then
	Browser("Browser").Page("Page").WebButton("Work").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,สร้าง ชิ้นงาน page loading for long")
End If

Browser("Browser").Page("Page").WebButton("สร้าง").Click


Browser("Browser").Page("Page").WebEdit("workItemCd").Set workItemCd @@ hightlight id_;_Browser("Browser").Page("Page").WebEdit("workItemCd")_;_script infofile_;_ZIP::ssf16.xml_;_
Browser("Browser").Page("Page").WebEdit("workItemName_2").Set workItemName @@ hightlight id_;_Browser("Browser").Page("Page").WebEdit("workItemName 2")_;_script infofile_;_ZIP::ssf17.xml_;_

Browser("Browser").Page("Page").WebElement("ชื่อกลุ่มงาน").Click

Browser("Browser").Page("Page").WebEdit("workgroupName").Set workgroupName
Browser("Browser").Page("Page").WebButton("ค้นหากลุ่มงาน").Click
wait 2
Browser("Browser").Page("Page").WebElement("WebTable").DoubleClick
Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click
Browser("Browser").Page("Page").WebButton("เลือก").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("เลือก").Click

CaptureScreenShot("")
Call WriteLog("Create,Gift page loading completed","")
'===================================End สร้าง ชิ้นงาน =======================================

'===================================Start เลือก ชิ้นงาน =====================================
wait 2
If  Browser("Browser").Page("Page").WebButton("Work").Exist(50) Then
	Browser("Browser").Page("Page").WebButton("Work").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("Work").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,เลือก ชิ้นงาน page loading for long")
End If

Browser("Browser").Page("Page").WebEdit("workItemCd_2").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("workItemCd_2").Set workItemCd

Browser("Browser").Page("Page").WebEdit("workItemName").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("workItemName").Set workItemName

Browser("Browser").Page("Page").WebButton("ค้นหาชิ้นงาน").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ค้นหาชิ้นงาน").Click
wait 2
Browser("Browser").Page("Page").WebElement("WebTable_2").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("WebTable_2").DoubleClick

CaptureScreenShot("")
Call WriteLog("Select,Gift page loading completed","")
'===================================End เลือก ชิ้นงาน =======================================

'===================================Start ช่องทางการแลกของกำนัล =============================

If  Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").Exist(50) Then
	Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").WaitProperty "visible",True, 10000
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,ช่องทางการแลกของกำนัล page loading for long")
End If
Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").WaitProperty "visible",True, 10000
ccMobileChannelFlg = Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").GetROProperty("checked")

If ccMobileChannelFlg = 0 Then
	Browser("Browser").Page("Page").WebCheckBox("ccMobileChannelFlg").Set "ON"
End If


Browser("Browser").Page("Page").WebElement("MobileApp").Click

Browser("Browser").Page("Page").WebElement("Display & Redeem").Click

Browser("Browser").Page("Page").WebEdit("ccMshortDetail").Set ccMshortDetail @@ hightlight id_;_Browser("Browser").Page("Page").WebEdit("ccMshortDetail")_;_script infofile_;_ZIP::ssf18.xml_;_
Browser("Browser").Page("Page").WebEdit("ccMdetail").Set ccMdetail @@ hightlight id_;_Browser("Browser").Page("Page").WebEdit("ccMdetail")_;_script infofile_;_ZIP::ssf19.xml_;_


Browser("Browser").Page("Page").WebEdit("ccMexpire").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("ccMexpire").Set ccMexpire

Browser("Browser").Page("Page").WebElement("ระยะใช้งาน").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("ระยะใช้งาน").Click

Browser("Browser").Page("Page").WebElement(Usageperiod).WaitProperty "visible",True, 10000
wait 2
Browser("Browser").Page("Page").WebElement(Usageperiod).Click

Browser("Browser").Page("Page").WebButton("บันทึกของกำนัล").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("บันทึกของกำนัล").Click

Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

CaptureScreenShot("")
Call WriteLog("Select,ช่องทางการแลกของกำนัล page loading completed","")

'===================================End ช่องทางการแลกของกำนัล =============================

'========================================Start Insert Image ============================
If  Browser("Browser").Page("Page").WebButton("แนบรูป").Exist(50) Then
	Browser("Browser").Page("Page").WebButton("แนบรูป").Click
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	Browser("Browser").Page("Page").WebFile("imgFile").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebFile("imgFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg"
	Browser("Browser").Page("Page").WebFile("imgThumb").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser").Page("Page").WebFile("imgThumb")_;_script infofile_;_ZIP::ssf22.xml_;_
	Browser("Browser").Page("Page").WebFile("infoFile").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg"
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Insert Image page loading for long")
End If
wait 2
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
Browser("Browser").Page("Page").WebButton("Upload").Click
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000


CaptureScreenShot("")
Call WriteLog("Insert,Image page loading completed","")
'========================================End Insert Image =====================================

'====================================Start บันทึกเงื่อนไขการชำระของกำนัล =============================

If  Browser("Browser").Page("Page").WebButton("สร้างเงื่อนไขการชำระ").Exist(50) Then
	Browser("Browser").Page("Page").WebButton("สร้างเงื่อนไขการชำระ").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,บันทึกเงื่อนไขการชำระของกำนัล  page loading for long")
End If
Browser("Browser").Page("Page").WebElement("WebElement_Payment").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("WebElement_Payment").Click
Browser("Browser").Page("Page").WebElement("คะแนนอย่างเดียว").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("คะแนนอย่างเดียว").Click
Browser("Browser").Page("Page").WebEdit("points").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("points").Set Points
Browser("Browser").Page("Page").WebButton("บันทึก_เปย์เม้นท์").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("บันทึก_เปย์เม้นท์").Click
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click


CaptureScreenShot("")
Call WriteLog("Save,บันทึกเงื่อนไขการชำระของกำนัล page loading completed","")
'====================================End บันทึกเงื่อนไขการชำระของกำนัล =============================

'====================================Start บันทึกของกำนัลคงคลัง =================================
If  Browser("Browser").Page("Page").Link("Product On-Hand").Exist(50) Then
	Browser("Browser").Page("Page").Link("Product On-Hand").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,บันทึกของกำนัลคงคลัง  page loading for long")
End If
wait 2
Browser("Browser").Page("Page").WebButton("เพิ่ม").WaitProperty "visible",True, 10000
If Browser("Browser").Page("Page").WebButton("เพิ่ม").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("เพิ่ม").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("เพิ่ม").Click
End If


Browser("Browser").Page("Page").WebElement("ประเภทรายการ").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("ประเภทรายการ").Click
Browser("Browser").Page("Page").WebElement(Item_Type).Click

If Browser("Browser").Page("Page").WebEdit("qty").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("qty").Set Unit
End If

If  Browser("Browser").Page("Page").WebEdit("comment").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("comment").Set Detail_treasury
End If

Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click


CaptureScreenShot("")
Call WriteLog("Save,บันทึกของกำนัลคงคลัง page loading completed","")
'====================================End บันทึกของกำนัลคงคลัง =================================

'====================================Start เปิดใช้งานของกำนัล =================================



If  Browser("Browser").Page("Page").WebElement("WebElement_Status").Exist(50) Then
	Browser("Browser").Page("Page").WebElement("WebElement_Status").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,เปิดใช้งานของกำนัล  page loading for long")
End If
wait 2
Browser("Browser").Page("Page").WebElement(Gift_Status).Click

Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

CaptureScreenShot("")
Call WriteLog("Open,เปิดใช้งานของกำนัล page loading completed","")

'====================================End เปิดใช้งานของกำนัล =================================


'====================================Start ค้นหาของกำนัล ====================================

If  Browser("Browser").Page("Page").Link("ของกำนัล").Exist(50) Then
	Browser("Browser").Page("Page").Link("ของกำนัล").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,ค้นหาของกำนัล  page loading for long")
End If
 @@ hightlight id_;_Browser("Browser").Page("Page").Link("ของกำนัล")_;_script infofile_;_ZIP::ssf27.xml_;_
If  Browser("Browser").Page("Page").WebEdit("searchProductName").Exist(5) Then
	wait 3
 	Browser("Browser").Page("Page").WebEdit("searchProductName").Set ProductName 
End If
Browser("Browser").Page("Page").WebButton("ค้นหา").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebButton("ค้นหา").Click

 CaptureScreenShot("")
 Call WriteLog("Search,ค้นหาของกำนัล page loading completed","")
 '====================================End ค้นหาของกำนัล ====================================
 
 
 
 
 
 
 
 
 
 

