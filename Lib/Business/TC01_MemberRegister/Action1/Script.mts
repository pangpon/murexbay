﻿
'======================================Start paramiter============================================
dataRow = Parameter("DTRow")
ApplicationNo = datatable.Value("ApplicationNo",dtglobalsheet)
CitizenID = datatable.Value("CitizenID",dtglobalsheet)
Prefix = datatable.Value("Prefix",dtglobalsheet)
Firstname = datatable.Value("Firstname",dtglobalsheet)
Lastname = datatable.Value("Lastname",dtglobalsheet)
DateOfBirth = datatable.Value("DateOfBirth",dtglobalsheet)
HouseNo = datatable.Value("HouseNo",dtglobalsheet)
ZipCode = datatable.Value("ZipCode",dtglobalsheet)
Province = datatable.Value("Province",dtglobalsheet)
District = datatable.Value("District",dtglobalsheet)
SubDistrict = datatable.Value("SubDistrict",dtglobalsheet)
MobileNumber = datatable.Value("MobileNumber",dtglobalsheet)
Email = datatable.Value("Email",dtglobalsheet)
Maritalstatus = datatable.Value("Maritalstatus",dtglobalsheet)
NumOfFamily = datatable.Value("NumOfFamily",dtglobalsheet)
NumOfChild = datatable.Value("NumOfChild",dtglobalsheet)
Child = datatable.Value("Child",dtglobalsheet)
Education = datatable.Value("Education",dtglobalsheet)
Occupation = datatable.Value("Occupation",dtglobalsheet)
MonthlyIncome = datatable.Value("MonthlyIncome",dtglobalsheet)
RegisteStatus = datatable.Value("RegisterStatus",dtglobalsheet)


'======================================End paramiter==========================================



RunAction "MemberLogin [Loyalty]", oneIteration
''======================================Start Login============================================
'OpenWeb_IE "http://10.102.63.33:8080/Loyalty/login.htm"
'
'If  Browser("Browser").Page("Page").WebEdit("j_username").Exist(10) Then
'	Browser("Browser").Page("Page").WebEdit("j_username").WaitProperty "visible",True, 10000
'	Browser("Browser").Page("Page").WebEdit("j_username").Set Username @@ script infofile_;_ZIP::ssf1.xml_;_
'	Browser("Browser").Page("Page").WebEdit("j_password").WaitProperty "visible",True, 10000
'	Browser("Browser").Page("Page").WebEdit("j_password").Set Password @@ script infofile_;_ZIP::ssf3.xml_;_
'	Browser("Browser").Page("Page").WebButton("Login").WaitProperty "visible",True, 10000
'	Browser("Browser").Page("Page").WebButton("Login").Click
'else
'	Browser("Browser").Page("Page").WebEdit("j_username").WaitProperty "visible",True, 10000
'	Browser("Browser").Page("Page").WebEdit("j_username").Set Username @@ script infofile_;_ZIP::ssf1.xml_;_
'	Browser("Browser").Page("Page").WebEdit("j_password").WaitProperty "visible",True, 10000
'	Browser("Browser").Page("Page").WebEdit("j_password").Set Password @@ script infofile_;_ZIP::ssf3.xml_;_
'	Browser("Browser").Page("Page").WebButton("Login").WaitProperty "visible",True, 10000
'	Browser("Browser").Page("Page").WebButton("Login").Click
'End IF
'
'If  Browser("Browser").Page("Page").WebButton("Clear Login").Exist(5) Then
'	Browser("Browser").Page("Page").WebButton("Clear Login").Click
'End If
'
'If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) Then
'	Browser("Browser").Page("Page").WebButton("ตกลง").Click
'	Browser("Browser").Page("Page").WebButton("Login").Click
'End If
'CaptureScreenShot("")
'Call WriteLog("Login,page loading completed","")
''======================================End Login============================================

'======================================Start SearchCustomer=================================

If  Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Exist(10) Then
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,SearchCustomer page loading for long")
End If
CaptureScreenShot("")
Call WriteLog("Timeout,Search Customer page loading for long","")
If Browser("Browser").Page("Page").WebButton("WebButton").Exist(2) Then	
	Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Click @@ hightlight id_;_Browser("Browser").Page("Page 2").Link("ลูกค้า/สมาชิก")_;_script infofile_;_ZIP::ssf146.xml_;_
	Browser("Browser").Page("Page").Link("ค้นหาลูกค้า/สมาชิก").Click
else
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Click @@ hightlight id_;_Browser("Browser").Page("Page 2").Link("ลูกค้า/สมาชิก")_;_script infofile_;_ZIP::ssf146.xml_;_
	Browser("Browser").Page("Page").Link("ค้นหาลูกค้า/สมาชิก").Click
End If
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
CaptureScreenShot("")
Call WriteLog("SearchCustomer,page loading completed","")
Browser("Browser").Page("Page").WebButton("ค้นหา").WaitProperty "visible",True, 10000
 If Browser("Browser").Page("Page").WebButton("ค้นหา").Exist(5) Then	 
	Browser("Browser").Page("Page").WebEdit("citizenId").Set CitizenID @@ hightlight id_;_Browser("Browser").Page("Page 2").WebElement("My Taskค้นหาลูกค้า/สมาชิก×")_;_script infofile_;_ZIP::ssf157.xml_;_
	wait 2
	Browser("Browser").Page("Page").WebElement("WebElement").Click
	Browser("Browser").Page("Page").WebElement("ไม่ใช่").Click @@ hightlight id_;_Browser("Browser").Page("Page 2").WebElement(".avatar-circle { -moz-border-r 2")_;_script infofile_;_ZIP::ssf159.xml_;_
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	If Browser("Browser").Page("Page").WebElement("Not_Data").Exist(5) Then
		CaptureScreenShot("")
		Call Fnc_ExitIteration("Timeout,Data not found")		
	End If
else
	Browser("Browser").Page("Page").WebElement("WebElement").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebEdit("citizenId").Set CitizenID
	wait 2
	Browser("Browser").Page("Page").WebElement("WebElement").Click
	Browser("Browser").Page("Page").WebElement("ไม่ใช่").Click @@ hightlight id_;_Browser("Browser").Page("Page 2").WebElement(".avatar-circle { -moz-border-r 2")_;_script infofile_;_ZIP::ssf159.xml_;_
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click	
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	If Browser("Browser").Page("Page").WebElement("Not_Data").Exist(5) Then
		CaptureScreenShot("")
		Call Fnc_ExitIteration("Timeout,Data not found")		
	End If
 
 End If
 

 
'======================================End SearchCustomer============================================ 

'======================================Start Creat_Customer==========================================
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
Browser("Browser").Page("Page").WebButton("สร้างใบสมัคร").WaitProperty "visible",True, 10000

If  Browser("Browser").Page("Page").WebButton("สร้างใบสมัคร").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("สร้างใบสมัคร").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("สร้างใบสมัคร").Click
	wait 5
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	 If Browser("Browser").Page("Page").WebElement("ลูกค้าท่านนี้ได้ยื่นใบสมัครแล้ว").Exist(5) Then
		CaptureScreenShot("")
		Call Fnc_ExitIteration("Timeout,The client already submitted the appication ")
	End If
	
else
	Browser("Browser").Page("Page").WebButton("สร้างใบสมัคร").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("สร้างใบสมัคร").Click
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
 	If Browser("Browser").Page("Page").WebElement("ลูกค้าท่านนี้ได้ยื่นใบสมัครแล้ว").Exist(5) Then
 		CaptureScreenShot("")
		Call Fnc_ExitIteration("Timeout,The client already submitted the appication ")
	End If
End If

Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
CaptureScreenShot("")
Call WriteLog("Save/Edit,Customer page loading completed","")
wait 2

'======================================Start Insurance information===========================
Browser("Browser").Page("Page").WebEdit("firstName").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("firstName").Set Firstname
Browser("Browser").Page("Page").WebEdit("lastName").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("lastName").Set Lastname
Browser("Browser").Page("Page").WebEdit("citizenId_2").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("citizenId_2").Set CitizenID
Browser("Browser").Page("Page").WebEdit("birthDate").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("birthDate").Set DateOfBirth
Browser("Browser").Page("Page").WebEdit("birthDate").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebElement("1. ข้อมูลผู้เอาประกัน").Click
CaptureScreenShot("")
Call WriteLog("Insurance information,page loading completed","")
'======================================End Insurance information==============================

'======================================Start Address==========================================

If Browser("Browser").Page("Page").WebEdit("houseNum").Exist(3) Then
	Browser("Browser").Page("Page").WebEdit("houseNum").WaitProperty "visible",True, 10000
	wait 2
	Browser("Browser").Page("Page").WebEdit("houseNum").Set HouseNo
else
	Browser("Browser").Page("Page").WebEdit("houseNum").WaitProperty "visible",True, 10000
	Wait 2
	Browser("Browser").Page("Page").WebEdit("houseNum").Set HouseNo
End If

wait 2
	Browser("Browser").Page("Page").WebElement("Prefix2").WaitProperty "visible",True, 2000
	Browser("Browser").Page("Page").WebElement("Prefix2").Click


If  Browser("Browser").Page("Page").WebElement("No matches found").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("No matches found").WaitProperty "visible",True, 10000
	wait 1
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Address page loading for long")
End If


If  Browser("Browser").Page("Page").WebElement(Prefix).Exist(5) Then
	Browser("Browser").Page("Page").WebElement(Prefix).WaitProperty "visible",True, 2000
	wait 2
	Browser("Browser").Page("Page").WebElement(Prefix).Click
	else
	Browser("Browser").Page("Page").WebElement(Prefix).WaitProperty "visible",True, 2000
	wait 2
	Browser("Browser").Page("Page").WebElement(Prefix).Click
End If


wait 2
Browser("Browser").Page("Page").WebEdit("registerNo").WaitProperty "visible",True, 10000
Browser("Browser").Page("Page").WebEdit("registerNo").Set ApplicationNo	

Browser("Browser").Page("Page").WebElement("ค้นหารหัสไปรษณีย์").WaitProperty "visible",True, 10000
If  Browser("Browser").Page("Page").WebElement("ค้นหารหัสไปรษณีย์").Exist(5) Then	
	Browser("Browser").Page("Page").WebElement("ค้นหารหัสไปรษณีย์").Click
End If

If  Browser("Browser").Page("Page").WebEdit("zipcode").Exist(5) Then	
	Browser("Browser").Page("Page").WebEdit("zipcode").Set ZipCode
End If

If  Browser("Browser").Page("Page").WebEdit("province").Exist(5) Then @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("zipcode")_;_script infofile_;_ZIP::ssf162.xml_;_
	Browser("Browser").Page("Page").WebEdit("province").Set Province
End If

If  Browser("Browser").Page("Page").WebEdit("amphur").Exist(5) Then @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("zipcode")_;_script infofile_;_ZIP::ssf162.xml_;_
	Browser("Browser").Page("Page").WebEdit("amphur").Set District
End If

If  Browser("Browser").Page("Page").WebEdit("tambon").Exist(5) Then @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("amphur")_;_script infofile_;_ZIP::ssf164.xml_;_
	Browser("Browser").Page("Page").WebEdit("tambon").Set SubDistrict
End If
	
If  Browser("Browser").Page("Page").WebButton("ค้นหาที่อยู่").Exist(5) Then @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("amphur")_;_script infofile_;_ZIP::ssf164.xml_;_
	Browser("Browser").Page("Page").WebButton("ค้นหาที่อยู่").Click
else
	Browser("Browser").Page("Page").WebButton("ค้นหาที่อยู่").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("ค้นหาที่อยู่").Click
End If

wait 2
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
If Browser("Browser").Page("Page").WebButton("เลือก").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("รายการ").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	wait 2
	Browser("Browser").Page("Page").WebButton("เลือก").WaitProperty "visible",True, 10000	
	Browser("Browser").Page("Page").WebButton("เลือก").Click
else
	wait 2
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	Browser("Browser").Page("Page").WebElement("รายการ").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("เลือก").WaitProperty "visible",True, 10000	
	Browser("Browser").Page("Page").WebButton("เลือก").Click

End If


If  Browser("Browser").Page("Page").WebEdit("mobilePhone").Exist(5) Then @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("amphur")_;_script infofile_;_ZIP::ssf164.xml_;_
	Browser("Browser").Page("Page").WebEdit("mobilePhone").Set MobileNumber
End If

If  Browser("Browser").Page("Page").WebEdit("emailAddr").Exist(5) Then @@ hightlight id_;_Browser("Browser").Page("Page 2").WebEdit("amphur")_;_script infofile_;_ZIP::ssf164.xml_;_
	Browser("Browser").Page("Page").WebEdit("emailAddr").Set Email
End If
CaptureScreenShot("")
Call WriteLog("Save/Edit,Address completed","")
'======================================End Address==========================================


'======================================Start Status / Family================================
Browser("Browser").Page("Page").WebElement("select_Status").Click
If  Browser("Browser").Page("Page").WebElement(Maritalstatus).Exist(5) Then
	Browser("Browser").Page("Page").WebElement(Maritalstatus).Click
End If

If NumOfChild = "มี" Then
	Browser("Browser").Page("Page").WebEdit("familyNum").Set NumOfFamily
	Browser("Browser").Page("Page").WebElement(NumOfChild).Click @@ hightlight id_;_Browser("Browser").Page("Page 2").WebRadioGroup("childFlg")_;_script infofile_;_ZIP::ssf171.xml_;_
	Browser("Browser").Page("Page").WebEdit("childNum").Set Child
else
	Browser("Browser").Page("Page").WebEdit("familyNum").Set NumOfFamily
	Browser("Browser").Page("Page").WebElement(NumOfChild).Click
End If
CaptureScreenShot("")
Call WriteLog("Save/Edit,Status / Family completed","")

'======================================End Status / Family============================

'======================================Start Education================================

If  Browser("Browser").Page("Page").WebElement("Education").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("Education").Click
	Browser("Browser").Page("Page").WebElement(Education).Click
Else
	Browser("Browser").Page("Page").WebElement("Education").WaitProperty "visible",false, 10000
	Browser("Browser").Page("Page").WebElement("Education").Click
	Browser("Browser").Page("Page").WebElement(Education).Click
End If

If  Browser("Browser").Page("Page").WebElement("Career").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("Career").Click
	Browser("Browser").Page("Page").WebElement(Occupation).Click
Else
	Browser("Browser").Page("Page").WebElement("Career").WaitProperty "visible",false, 10000
	Browser("Browser").Page("Page").WebElement("Career").Click
	Browser("Browser").Page("Page").WebElement(Occupation).Click
End If

If  Browser("Browser").Page("Page").WebElement("Income").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("Income").Click
	Browser("Browser").Page("Page").WebElement(MonthlyIncome).Click
Else
	Browser("Browser").Page("Page").WebElement("Income").WaitProperty "visible",false, 10000
	Browser("Browser").Page("Page").WebElement("Income").Click
	Browser("Browser").Page("Page").WebElement(MonthlyIncome).Click
End If

CaptureScreenShot("")
Call WriteLog("Save/Edit, Education / Family completed","")

'======================================End Education===============================

'======================================Start Submit================================

If  Browser("Browser").Page("Page").WebButton("Submit").Exist(5) Then	
	Browser("Browser").Page("Page").WebButton("Submit").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("Submit").Click
else
	Browser("Browser").Page("Page").WebButton("Submit").WaitProperty "visible",True, 10000
	Browser("Browser").Page("Page").WebButton("Submit").Click
End If

Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 60000
Browser("Browser").Page("Page").WebElement("สถานะการสมัคร : อนุมัติ").WaitProperty "visible",True, 10000

actualRegisterStatus = Browser("Browser").Page("Page").WebElement("สถานะการสมัคร : อนุมัติ").GetROProperty("innertext")
If 	Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
	Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
	CaptureScreenShot("")
	Call WriteLog("SubmitRegister,SubmitRegister Successfully","")
	Browser("Browser").Page("Page").WebButton("ตกลง").Click
else
	Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",True, 10000
	CaptureScreenShot("")
	Call WriteLog("SubmitRegister,SubmitRegister Successfully","")
	Browser("Browser").Page("Page").WebButton("ตกลง").Click
End If

Call Result_CompareText(actualRegisterStatus,RegisteStatus,"SubmitRegister","SubmitRegister Successfully")


'======================================End Submit================================


