﻿'============================== Start data table to variable ================================

dataRow = Parameter("DTRow")
ProductName = DataTable("ProductName",dtglobalsheet)
Item_Type = DataTable("Item_Type",dtglobalsheet)
Gift_Status = DataTable("Gift_Status",dtglobalsheet)
Gift_Status2 = DataTable("Gift_Status2",dtglobalsheet)
Title = DataTable("Title",dtglobalsheet)
Detail_treasury = DataTable("CommentQty",dtglobalsheet)
ShortProductName = DataTable("DocumentName",dtglobalsheet)
Product_Category = DataTable("Product_Category",dtglobalsheet)
Points = DataTable("Point",dtglobalsheet)
 
'============================== End data table to variable ================================

RunAction "MemberLogin", oneIteration, dataRow


'============================== Start Menu องค์ความรู้  ================================

If  Browser("Browser").Page("Page").Link("จัดการองค์ความรู้").Exist(50) Then	
	Browser("Browser").Page("Page").Link("จัดการองค์ความรู้").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Menu loading for long")
End If

Browser("Browser").Page("Page").Link("บริการไทยไลฟ์การ์ด").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").Link("บริการไทยไลฟ์การ์ด").Click
wait 5
If Browser("Browser").Page("Page").WebButton("สร้าง").Exist(50) Then
	Browser("Browser").Page("Page").WebButton("สร้าง").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebButton("สร้าง").Click
	wait 3
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
	If Browser("Browser").Page("Page").WebElement("ไม่พบข้อมูลที่ต้องการในระบบ").Exist(5) Then
	Call Fnc_ExitIteration("Timeout,Search Customer loading for long")
	End If
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Menu loading for long")
End If


If  Browser("Browser").Page("Page").WebEdit("WebEdit").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
	Browser("Browser").Page("Page").WebEdit("WebEdit").Set ProductName
End If



If  Browser("Browser").Page("Page").WebElement("ประเภทข้อมูล").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("ประเภทข้อมูล").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("ประเภทข้อมูล").Click
	wait 2
	Browser("Browser").Page("Page").WebElement(Item_Type).Click
End If



If Browser("Browser").Page("Page").WebElement("สถานะการใช้งาน").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("สถานะการใช้งาน").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("สถานะการใช้งาน").Click
	wait 2
	Browser("Browser").Page("Page").WebElement(Gift_Status).Click
End If

Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click

If  Browser("Browser").Page("Page").WebElement("ดำเนินการเรียบร้อย").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("ดำเนินการเรียบร้อย").Click
End If

Browser("Browser").Page("Page").WebButton("ตกลง").Click

Name_CustomerCompare = "เจ้าหน้าที่โลคัส"
'Date_CustomerCompare = "04/03/2562"
wait 3
Name_Customer = Browser("Browser").Page("Page").WebEdit("ผู้สร้าง").GetROProperty("Value")
Date_Customer = Browser("Browser").Page("Page").WebEdit("วันที่สร้าง").GetROProperty("Value")
Date_Customer1 = Mid(Date_Customer,1,10) 

If Name_CustomerCompare = Name_Customer Then

	else
		Call Fnc_ExitIteration("Data does not match")	
		Call Fnc_ExitIteration("Timeout,Data does not match")	
End If

Call WriteLog("Menu องค์ความรู้ Completed","")	
CaptureScreenShot("")
'============================== End Menu องค์ความรู้  ================================

'============================== Start Search องค์ความรู้  ============================

If  Browser("Browser").Page("Page").Link("ค้นหา").Exist(50) Then
	Browser("Browser").Page("Page").Link("ค้นหา").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,Search Customer loading for long")
End If

Browser("Browser").Page("Page").WebEdit("title").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("title").Set Title

Browser("Browser").Page("Page").WebButton("ค้นหา").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ค้นหา").Click

Browser("Browser").Page("Page").WebElement("WebTable").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebElement("WebTable").DoubleClick

Call WriteLog("Search องค์ความรู้ Completed","")	
CaptureScreenShot("")
'============================== End Search องค์ความรู้  ===============================

'=================================== Start แก้ไขสรุป  ===============================

Browser("Browser").Page("Page").WebEdit("สรุป").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("สรุป").Set Detail_treasury
Browser("Browser").Page("Page").WebEdit("สรุป").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("บันทึกสรุป").Click
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

'=================================== End แก้ไขสรุป  =================================

'=================================== Start เอกสารแนบ ===============================

If  Browser("Browser").Page("Page").Link("เอกสารแนบ").Exist(50) Then
	Browser("Browser").Page("Page").Link("เอกสารแนบ").Click
else
	CaptureScreenShot("")
	Call Fnc_ExitIteration("Timeout,แนบเอกสาร loading for long")
End If

Browser("Browser").Page("Page").WebButton("สร้างแนบเอกสาร").WaitProperty "visible",true, 10000
wait 3
Browser("Browser").Page("Page").WebButton("สร้างแนบเอกสาร").Click


Browser("Browser").Page("Page").WebButton("select_Image").WaitProperty "visible",true, 10000
wait 2
Browser("Browser").Page("Page").WebButton("select_Image").Click @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("WebButton")_;_script infofile_;_ZIP::ssf23.xml_;_
wait 3
Browser("Browser").Page("Page").WebFile("file").Set "D:\Loyalty\Data\Picture\Redem300pt.jpg" @@ hightlight id_;_Browser("Browser").Page("Page 2").WebFile("file")_;_script infofile_;_ZIP::ssf24.xml_;_

If  Browser("Browser").Page("Page").WebButton("อัพโหลด").Exist(5) Then
	wait 2
	Browser("Browser").Page("Page").WebButton("อัพโหลด").Click
End IF

If  Browser("Browser").Page("Page").WebButton("ตกลงอัพไฟล์").Exist(5) Then
	wait 2
	Browser("Browser").Page("Page").WebButton("ตกลงอัพไฟล์").Click
End IF @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("อัพโหลด")_;_script infofile_;_ZIP::ssf25.xml_;_
 @@ hightlight id_;_Browser("Browser").Page("Page 2").WebButton("ตกลง")_;_script infofile_;_ZIP::ssf26.xml_;_


Browser("Browser").Page("Page").WebCheckBox("mainFlg").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebCheckBox("mainFlg").Set "ON"

Browser("Browser").Page("Page").WebCheckBox("sendDocTypeFlg").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebCheckBox("sendDocTypeFlg").Set "ON"

Browser("Browser").Page("Page").WebEdit("attName").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("attName").Set ShortProductName

Browser("Browser").Page("Page").WebEdit("descp").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("descp").Set Product_Category

If  Browser("Browser").Page("Page").WebButton("บันทึก_ไฟล์แนบ").Exist(5) Then
	wait 5
	Browser("Browser").Page("Page").WebButton("บันทึก_ไฟล์แนบ").Click
End If
wait 1
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",False, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

If  Browser("Browser").Page("Page").Link("เอกสารหลัก").Exist(10) Then
	Browser("Browser").Page("Page").Link("เอกสารหลัก").Click
End If

Call WriteLog("Menu เอกสารแนบ Completed","")	
CaptureScreenShot("")
'=================================== End เอกสารแนบ ===============================

'=================================== Start คำสำคัญ ===============================

If  Browser("Browser").Page("Page").Link("คำสำคัญ").Exist(10) Then
	Browser("Browser").Page("Page").Link("คำสำคัญ").Click
End If

Browser("Browser").Page("Page").WebButton("สร้าง_คำสำคัญ").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("สร้าง_คำสำคัญ").Click
Browser("Browser").Page("Page").WebEdit("keyword").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebEdit("keyword").Set Points
Browser("Browser").Page("Page").WebButton("บันทึก_คำสำคัญ").Click


If  Browser("Browser").Page("Page").WebButton("ตกลง").Exist(10) Then
	Browser("Browser").Page("Page").WebButton("ตกลง").Click
End If

Call WriteLog("Menu คำสำคัญ Completed","")	
CaptureScreenShot("")
'=================================== End คำสำคัญ ===============================

'=================================== Startเปิดใช้งาน =============================


If Browser("Browser").Page("Page").WebElement("สถานะการใช้งาน").Exist(5) Then
	Browser("Browser").Page("Page").WebElement("สถานะการใช้งาน").WaitProperty "visible",true, 10000
	Browser("Browser").Page("Page").WebElement("สถานะการใช้งาน").Click
	wait 2
	Browser("Browser").Page("Page").WebElement(Gift_Status2).Click
End If

Browser("Browser").Page("Page").WebButton("บันทึก").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("บันทึก").Click
Browser("Browser").Page("Page").WebButton("ตกลง").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ตกลง").Click

Call WriteLog("Menu เปิดใช้งาน Completed","")	
CaptureScreenShot("")
'=================================== End เปิดใช้งาน ===============================

'=================================== Startค้นหาองค์ความรู้ ==========================


Browser("Browser").Page("Page").Link("บริการไทยไลฟ์การ์ด_องค์ความรู้").Click


Browser("Browser").Page("Page").Link("ค้นหา_องค์ความรู้").Click
Browser("Browser").Page("Page").WebEdit("keyword_องค์ความรู้").WaitProperty "visible",true, 10000
wait 2
Browser("Browser").Page("Page").WebEdit("keyword_องค์ความรู้").Set Points
Browser("Browser").Page("Page").WebButton("ค้นหา_องค์ความรู้2").WaitProperty "visible",true, 10000
Browser("Browser").Page("Page").WebButton("ค้นหา_องค์ความรู้2").Click

Call WriteLog("Menu ค้นหาองค์ความรู้ Completed","")	
CaptureScreenShot("")

'=================================== End ค้นหาองค์ความรู้ ==========================
