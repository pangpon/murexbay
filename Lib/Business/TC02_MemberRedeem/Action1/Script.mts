﻿
dataRow = Parameter("DTRow")
citizenID = DataTable("CitizenID",dtglobalsheet)
serviceChannel = DataTable("ServiceChannel",dtglobalsheet)
receiveConfirmChannel = DataTable("ReceiveConfirmChannel",dtglobalsheet)
quantity = DataTable("Quantity",dtglobalsheet)
'deliveryStatus = DataTable("DeliveryStatus",dtglobalsheet)
 
'============================== End data table to variable ================================

'OpenWeb_IE "http://10.102.60.33:8080/Loyalty/login.htm"
'OpenWeb_IE "http://10.102.63.33:8080/Loyalty/login.htm"
RunAction "MemberLogin", oneIteration, dataRow

'=================================== Start Home My Task ===================================

	Browser("Browser").Page("Page").WebElement("รายการ Activity").WaitProperty "visible",true, 10000
	Call WriteLog("Loading Home My Task Completed","")	
	CaptureScreenShot("")
'=================================== End Home My Task =====================================

'============================== Start Menu Search Customer/Member =========================
If Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Exist(3) Then
	Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Click
	If Browser("Browser").Page("Page").Link("ค้นหาลูกค้า/สมาชิก").Exist(3) = true  Then
		Browser("Browser").Page("Page").Link("ค้นหาลูกค้า/สมาชิก").Click
		Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	End If
	Call WriteLog("Click Search Customer/Member Completed","")	
	CaptureScreenShot("")
End If @@ hightlight id_;_Browser("Browser 3").Page("Page 2").Link("ลูกค้า/สมาชิก")_;_script infofile_;_ZIP::ssf18.xml_;_
'================================ End Menu Search Customer/Member =========================
 
'=============================== Start Search Customer/Member =============================
If Browser("Browser").Page("Page").WebEdit("citizenId").Exist(3) = true Then
	Browser("Browser").Page("Page").WebEdit("citizenId").Set citizenID
	If Browser("Browser").Page("Page").WebButton("ค้นหา").Exist(3) Then
		Browser("Browser").Page("Page").WebButton("ค้นหา").Click
	    Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	End If
	Call WriteLog("Entry CitizenID Search Completed","")	
	CaptureScreenShot("")
End If

tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebElement("tblloyalty_member_search_result").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,รายละเอียดสมาชิก loading for long","")
		Call Fnc_ExitIteration("Timeout,รายละเอียดสมาชิก loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
Call WriteLog("รายละเอียดสมาชิก loading completed","")

wait 3
If 	Browser("Browser").Page("Page").WebElement("tblloyalty_member_search_result").Exist(3) Then
	Browser("Browser").Page("Page").WebElement("tblloyalty_member_search_result").DoubleClick
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 60000
	Call WriteLog("Double Click On Search Result","")	
	CaptureScreenShot("")
End If
'=============================== End Search Customer/Member ===============================

'Browser("Browser").Page("Page").WebElement("Div_tb_redeemGifttab").Click
'================================= Start คะแนน/แลกของกำนัล ==================================
		wait 3
	  	tmp_timeout = 0
		Do until Browser("Browser").Page("Page").WebElement("Div_tb_redeemGifttab").Exist(5) = True
			Wait 1
			If tmp_timeout = 10 Then
				Call WriteLog("Timeout,คะแนน/แลกของกำนัล loading for long","")
				Call Fnc_ExitIteration("Timeout,คะแนน/แลกของกำนัล loading for long")
				CaptureScreenShot("")		'Capture Screen
				Exit Do
			End If
			tmp_timeout = tmp_timeout + 1
		Loop
	 Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Click
	 wait 3
	 Call WriteLog("Click On คะแนน/แลกของกำนัล","")	
	 If Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(3) Then
	 	Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
	 End If
	 Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	 Call WriteLog("Click On แลกของกำนัล","")	
	 CaptureScreenShot("")
'================================= End คะแนน/แลกของกำนัล =====================================

'================================= Start Create รายละเอียดการแลกของกำนัล ========================
wait 3
tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebButton("สร้าง").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,Create button not fuound","")
		Call Fnc_ExitIteration("Timeout,Create button not fuound")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
If Browser("Browser").Page("Page").WebButton("สร้าง").Exist(3) = true Then
	Browser("Browser").Page("Page").WebButton("สร้าง").Click
End If
'================================ End Create รายละเอียดการแลกของกำนัล ===========================

'================================ Start รายละเอียดการแลกของกำนัล ================================

Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 50000
	Call WriteLog("รายละเอียดสมาชิก loading completed","")
	CaptureScreenShot("")

tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebElement("รายละเอียดการแลกของกำนัล").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,รายละเอียดการแลกของกำนัล page loading for long","")
		Call Fnc_ExitIteration("Timeout,รายละเอียดการแลกของกำนัล page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
'================================ End รายละเอียดการแลกของกำนัล ================================== @@ hightlight id_;_Browser("Browser 4").Page("Page").WebButton("สร้าง")_;_script infofile_;_ZIP::ssf40.xml_;_

'================================ Start รายละเอียดสมาชิก =======================================
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
wait 3
If Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Exist(3) = true Then
	Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Click
	If Browser("Browser").Page("Page").WebElement(serviceChannel).Exist(3) Then
	Browser("Browser").Page("Page").WebElement(serviceChannel).Click
	If Browser("Browser").Page("Page").WebElement("frmLoyalty_redeem_result_rdmHeader").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("frmLoyalty_redeem_result_rdmHeader").Click
		If Browser("Browser").Page("Page").WebElement(receiveConfirmChannel).Exist(3) Then
			Browser("Browser").Page("Page").WebElement(receiveConfirmChannel).Click
		End If
		End If
	End If
End If
Call WriteLog("รายละเอียดสมาชิก loading completed","")	
CaptureScreenShot("")
'=============================== End รายละเอียดสมาชิก ==========================================

'================================ Start Select Address =====================================
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
wait 1
If Browser("Browser").Page("Page").WebElement("redeem_result_btnAddress").Exist(3)  = true Then
	Browser("Browser").Page("Page").WebElement("redeem_result_btnAddress").Click
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	If Browser("Browser").Page("Page").WebElement("รายการที่อยู่").Exist(3) Then
		wait 3
		If Browser("Browser").Page("Page").WebButton("modalAddr_btn_select_CommonAddress").Exist(10) = true Then
		Browser("Browser").Page("Page").WebButton("modalAddr_btn_select_CommonAddress").Click
		Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
		End If
	End If
End If @@ hightlight id_;_Browser("Browser 3").Page("Page 2").WebElement(".avatar-circle { -moz-border-r")_;_script infofile_;_ZIP::ssf48.xml_;_
Call WriteLog("Select Address Completed","")	
CaptureScreenShot("")
pointBalanceBefore = Browser("Browser").Page("Page").WebElement("redeem_result_lbl_pointTypeVal").GetROProperty("innertext")
'================================ End Select Address ======================================

'================================ Start Shopping Page Popup ===============================
wait 3
If Browser("Browser").Page("Page").WebElement("redeem_result_btn_shopping").Exist(0) Then
	Browser("Browser").Page("Page").WebElement("redeem_result_btn_shopping").Click
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
	tmp_timeout = 0
	Do until Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Exist(5) = True
		Wait 1
		If tmp_timeout = 10 Then
			Exit Do
		End If
		tmp_timeout = tmp_timeout + 1
	Loop
	wait 3
	If Browser("Browser").Page("Page").WebElement("ทั้งหมด").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("ทั้งหมด").Click
		If Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Exist(3) Then
			Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Click
			If Browser("Browser").Page("Page").WebElement("searchProductBtn").Exist(3) Then
			wait 1
				Browser("Browser").Page("Page").WebElement("searchProductBtn").Click
				If Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Exist(5) = true Then
					Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Highlight
					wait 3
					Browser("Browser").Page("Page").WebElement("loyaltySearchShoppingTbl").Click
				End If
			End If
		End If
	End If
End If
Call WriteLog("Click On Shopping Page Popup","")	
CaptureScreenShot("")
'=============================== End Shopping Page Popup ===================================

Browser("Browser").Page("Page").WebElement("loyaltySearchShopping").Click
'=============================== Check Point Before Redeem =================================

'=============================== Start Select gifts ========================================
wait 3 'quantity

If Browser("Browser").Page("Page").WebEdit("quantity").Exist(3) Then
	Browser("Browser").Page("Page").WebEdit("quantity").Set quantity
	wait 1
	If Browser("Browser").Page("Page").WebElement("กรุณาเลือก_2").Exist(3) Then
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก_2").Click
		If Browser("Browser").Page("Page").WebElement("จัดส่งไปรษณีย์").Exist(3) Then
			Browser("Browser").Page("Page").WebElement("จัดส่งไปรษณีย์").Click
			If Browser("Browser").Page("Page").WebButton("modalRedeemShopping_btnOk").Exist(3) Then
				Browser("Browser").Page("Page").WebButton("modalRedeemShopping_btnOk").Click
				'Browser("Browser").Page("Page").WebElement("loyUserOwnerSearchModal").Click
				wait 2
					If Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Exist(3) Then
					Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Click
					End If
			End If
		End If @@ hightlight id_;_Browser("Browser 3").Page("Page 2").WebButton("ยืนยันรายการ")_;_script infofile_;_ZIP::ssf52.xml_;_
	End If
End If
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 50000
Call WriteLog("Select gifts Completed","")	
CaptureScreenShot("") @@ hightlight id_;_Browser("Browser 3").Page("Page 2").WebElement("1")_;_script infofile_;_ZIP::ssf55.xml_;_

'=============================== End Select gifts ==========================================

'=============================== Start Redeem gifts ========================================
If Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Exist(3) Then
	Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Click
	Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 10000
		If Browser("Browser").Page("Page").WebButton("ใช่").Exist(5) Then
		Browser("Browser").Page("Page").WebButton("ใช่").Click	
		 If Browser("Browser").Page("Page").WebElement("ต้องการยืนยันหรือไม่?").Exist(3) Then
		 	Browser("Browser").Page("Page").WebElement("ต้องการยืนยันหรือไม่?").Click
		 	If Browser("Browser").Page("Page").WebButton("ใช่").Exist(3) Then
		 		Browser("Browser").Page("Page").WebButton("ใช่").Click
		 		Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 50000
				Browser("Browser").Page("Page").WebElement("ยืนยันการแลกของกำนัลเรียบร้อยแ").Click
				If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(3) Then
					Browser("Browser").Page("Page").WebButton("ตกลง").Click
				End If		
		 	End If
		 End If
		End If
End If
'=============================== End Redeem gifts ========================================

'=============================== Start Get Redeem gifts ==================================
Browser("Browser").Page("Page").WebElement("กรุณารอสักครู่...").WaitProperty "visible",false, 50000
wait 3
If Browser("Browser").Page("Page").WebElement("redeem_result_lbl_pointTypeVal").Exist(3) = true Then
	pointBalanceAfter = Browser("Browser").Page("Page").WebElement("redeem_result_lbl_pointTypeVal").GetROProperty("innertext")
End If
wait 3
If Browser("Browser").Page("Page").WebElement("loyaltyProductSelectedTbl").Exist(3) = true Then
	pointRedeem = Browser("Browser").Page("Page").WebElement("loyaltyProductSelectedTbl").GetROProperty("innertext")
End If
If Browser("Browser_2").Page("Page_3").WebElement("tblloyalty_redeem_tranID_item").Exist(3) = true Then
	transactionID = Browser("Browser_2").Page("Page_3").WebElement("tblloyalty_redeem_tranID_item").GetROProperty("innertext")
End If
'pointBalanceAfter = Browser("Browser").Page("Page").WebElement("redeem_result_lbl_pointTypeVal").GetROProperty("innertext")
'=============================== End Get Redeem gifts ===================================


'=============================== Start Convert & Compare Point =============================
actualBalanBefore = Replace(pointBalanceBefore,",","") ' Point คงเหลือก่อนทำการ Redeem
actualPointRedeem = Replace(pointRedeem,",","") ' Point ที่ใช้ Redeem
actualPointAfter = Replace(pointBalanceAfter,",","") ' Point คงเหลือหลังทำการ Redeem
actualPointAfter = CDBL(actualPointAfter)
actualBalanBefore = CDBL(actualBalanBefore)
actualPointRedeem = CDBL(actualPointRedeem)
actulaPointBalance = actualBalanBefore - actualPointRedeem
Call Result_CompareText(actualPointAfter,actulaPointBalance,"MemberRedeem","คะแนนที่ใช้"&" "& actualPointRedeem &" "&"คะแนนคงเหลือ"& " "& pointBalanceAfter &" "& "เลขที่รายการแลกของกำนัล" &" "& transactionID)

'=============================== End Convert & Compare Point =============================
