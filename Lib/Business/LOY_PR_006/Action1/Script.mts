﻿
memName = DataTable("Firstname",dtglobalsheet)
dataRow = Parameter("dataRow")

RunAction "MemberLogin", oneIteration, dataRow


tmp_timeout = 0
Do until Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Exist(5) = True
	Wait 1
	If tmp_timeout = 10 Then
		Call WriteLog("Timeout,Login page loading for long","")
		Call Fnc_ExitIteration("Timeout,Login page loading for long")
		CaptureScreenShot("")		'Capture Screen
		Exit Do

	End If
	tmp_timeout = tmp_timeout + 1
Loop
Call WriteLog("Login page loading completed","")
CaptureScreenShot("")

'==================================== เจ้าหน้าที่ CRM ค้นหาลูกค้าที่ต้องการแลกของกำนัล ======================================

If Browser("Browser").Page("Page").WebButton("toolbar_walkin").Exist(5) Then
		Browser("Browser").Page("Page").WebButton("toolbar_walkin").Click
End If

Wait 0.2

If Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Exist(10) Then
		Browser("Browser").Page("Page").Link("ลูกค้า/สมาชิก").Click
		Wait 0.1
		If Browser("Browser").Page("Page").Link("ค้นหาลูกค้า/สมาชิก").Exist(5) Then
				Browser("Browser").Page("Page").Link("ค้นหาลูกค้า/สมาชิก").Click
				Wait 0.1
				
		End If
		CaptureScreenShot("")
End If

Wait 0.1

If Browser("Browser").Page("Page").WebEdit("memName").Exist(5) Then
	Browser("Browser").Page("Page").WebEdit("memName").Click
	Wait 3
	Browser("Browser").Page("Page").WebEdit("memName").Set memName
	Wait 0.1
	Browser("Browser").Page("Page").WebButton("ค้นหา").Click
	Wait 0.5
End If



tmp_timeout = 0
Do until Browser("Browser").Page("Page").WebTable("สิ้นสุดสถานภาพ โดย CRM").RowCount > 1
	Wait 3
	If tmp_timeout = 10 Then
		Call WriteLog("Loading Home My Task page loading for long","")
		Call Fnc_ExitIteration("Search not false")
		CaptureScreenShot("")		'Capture Screen
		Exit Do
	End If
	tmp_timeout = tmp_timeout + 1
Loop
Call WriteLog("Loading Home My Task Completed","")	
CaptureScreenShot("")


If Browser("Browser").Page("Page").WebElement("WebTable01").Exist(10) Then
		Browser("Browser").Page("Page").WebElement("WebTable01").DoubleClick	
End If
Wait 0.1


'==================================== เจ้าหน้าที่ CRM ค้นหาลูกค้าที่ต้องการแลกของกำนัล ======================================

If Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Exist(5) Then
	Browser("Browser").Page("Page").Link("คะแนน/แลกของกำนัล").Click
	Wait 0.1
	If Browser("Browser").Page("Page").Link("แลกของกำนัล").Exist(5)	Then
			Browser("Browser").Page("Page").Link("แลกของกำนัล").Click
			Wait 0.1
			If Browser("Browser").Page("Page").WebButton("buttonสร้าง").Exist(10) Then
				Wait 0.1
				CaptureScreenShot("")
				Browser("Browser").Page("Page").WebButton("buttonสร้าง").Click
			End If
			

	End If
End If

Wait 10
If Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("กรุณาเลือก").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebElement("ติดต่อด้วยตนเอง").Click
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebElement("รถเข็น").Click
End If


'====================================2 เจ้าหน้าที่ CRM ค้นหาของกำนัลที่ต้องการ ======================================
Wait 3
'Shopping Page Popup

If Browser("Browser").Page("Page").WebElement("Shopping Page Popup").Exist(10) Then
	Browser("Browser").Page("Page").WebEdit("search_product_name").Click
	Wait 0.1
	Browser("Browser").Page("Page").WebEdit("search_product_name").Set "Gift Voucher Central 500 บาท"

End If

Wait 0.5

If Browser("Browser").Page("Page").WebElement("เลือกประเภท").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("เลือกประเภท").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebElement("ของกำนัล/บัตรกำนัล").Click
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("ค้นหา").Click

End If



'====================================2 เจ้าหน้าที่ CRM ทดสอบแลกของกำนัล ======================================

Wait 0.5
If Browser("Browser").Page("Page").WebElement("รถเข็นแดง").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("รถเข็นแดง").Click 
End If
Wait 0.1
If Browser("Browser").Page("Page").WebElement("เลือกของกำนัล").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("เลือกของกำนัล").Click
		Wait 0.1
		Browser("Browser").Page("Page").WebEdit("quantity").Set "1"
		
		If Browser("Browser").Page("Page").WebElement("เลือกวิธีส่งมอบ").Exist(5) Then
				Browser("Browser").Page("Page").WebElement("เลือกวิธีส่งมอบ").Click
				Wait 0.1
				Browser("Browser").Page("Page").WebElement("รับด้วยตนเอง").Click
				Wait 0.1
				Browser("Browser").Page("Page").WebElement("ค้นหาที่อยู่").Click
				If Browser("Browser").Page("Page").WebElement("ค้นหาศูนย์บริการลูกค้า").Exist(10) Then
						Browser("Browser").Page("Page").WebElement("ค้นหาศูนย์บริการลูกค้า").Click
						Wait 0.1
						Browser("Browser").Page("Page").WebEdit("cscServiceCenterName").Set "ส่วนลูกค้าสัมพันธ์ ชั้น 6 (สำนักงานใหญ่)"
						Wait 0.1
						Browser("Browser").Page("Page").WebButton("ค้นหา_2").Click
						Wait 0.1
						Browser("Browser").Page("Page").WebElement("เลือกส่วนลูกค้าสัมพันธ์ ชั้น 6 (สำนักงานใหญ่)").DoubleClick
						Wait 0.1
						If Browser("Browser").Page("Page").WebButton("ตกลง").Exist(5) Then
								Browser("Browser").Page("Page").WebButton("ตกลง").Click
						End If
						
				End If
				
		End If
		
End If

Wait 0.5

If Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Exist Then
		Browser("Browser").Page("Page").WebButton("ยืนยันรายการ").Click
End If

Wait 3

If Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Exist(10) Then
		Wait 0.5
		Browser("Browser").Page("Page").WebButton("ยืนยันทั้งหมด").Click
		Wait 0.1
		If Browser("Browser").Page("Page").WebElement("ต้องการยืนยันหรือไม่").Exist(10) Then
				CaptureScreenShot("")
				If Browser("Browser").Page("Page").WebButton("ใช่").Exist(5) Then
						Browser("Browser").Page("Page").WebButton("ใช่").Click
						Wait 0.1
				End If
					
		End If
End If
Wait 0.5

If Browser("Browser").Page("Page").WebElement("ยืนยันการแลกของกำนัลเรียบร้อยแ").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		mg = Browser("Browser").Page("Page").WebElement("ยืนยันการแลกของกำนัลเรียบร้อยแ").GetROProperty("innertext")
		Call WriteLog(""&mg,"")
		Wait 0.1
		
End If
Wait 0.5

If Browser("Browser").Page("Page").WebButton("ตกลง_2").Exist(10) Then
		Browser("Browser").Page("Page").WebButton("ตกลง_2").Click
End If

Wait 3
'====================================4 เจ้าหน้าที่ CRM ตรวจสอบการบันทึกเรื่องที่ให้บริการ ======================================

If Browser("Browser").Page("Page").Link("เรื่องที่ให้บริการ").Exist(10) Then
		Browser("Browser").Page("Page").Link("เรื่องที่ให้บริการ").Click
End If
Wait 0.5

'ตรวจสอบ 

numberNo = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,2)
Call WriteLog("เลขที่รับเรื่อง : "&numberNo,"")

nameCostomer = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,3)
Call WriteLog("ชื่อลูกค้า : "&nameCostomer,"")

redeemTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,4)
Call WriteLog("ประเภทเรื่อง : "&redeemTyp,"")

subredeemTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,5)
Call WriteLog("ประเภทเรื่องย่อย : "&subredeemTyp,"")

status  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,6)
Call WriteLog("สถานะ : "&status,"")

responSile   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,7)
Call WriteLog("ผู้รับผิดชอบ : "&responSile,"")

dateTime   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,8)
Call WriteLog("วันที่รับเรื่อง : "&dateTime,"")




'====================================5. เจ้าหน้าที่ CRM บันทึกผลการติดต่อ ======================================

If Browser("Browser").Page("Page").WebElement("ประวัติการติดต่อ").Exist(5) Then
		Browser("Browser").Page("Page").WebElement("ประวัติการติดต่อ").Click
		Wait 0.1
		Browser("Browser").Page("Page").Link("สรุปการติดต่อ").Click
		Wait 0.1
		Browser("Browser").Page("Page").Link("เรื่องให้บริการ").Click
		


'ระบบสร้างรายการเรื่องที่ให้บริการตามที่ทำรายการไว้
numberNo = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,2)
Call WriteLog("เลขที่รับเรื่อง : "&numberNo,"")

nameCostomer = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,3)
Call WriteLog("ชื่อลูกค้า : "&nameCostomer,"")

redeemTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,4)
Call WriteLog("ประเภทเรื่อง : "&redeemTyp,"")

subredeemTyp  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,5)
Call WriteLog("ประเภทเรื่องย่อย : "&subredeemTyp,"")

status  = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,6)
Call WriteLog("สถานะ : "&status,"")

responSile   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,7)
Call WriteLog("ผู้รับผิดชอบ : "&responSile,"")

dateTime   = Browser("Browser").Page("Page").WebTable("ลำดับ").GetCellData(2,8)
Call WriteLog("วันที่รับเรื่อง : "&dateTime,"")


End If

'กดปุ่ม Walk-In ที่ Dashboard เพื่อสิ้นสุดการติดต่อ

If Browser("Browser").Page("Page").WebButton("toolbar_walkin").Exist(10) Then
		Wait 0.1
		CaptureScreenShot("")
		Browser("Browser").Page("Page").WebButton("toolbar_walkin").Click
		Call WriteLog("สุดการติดต่อ","")
End If
